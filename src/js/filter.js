'use strict';

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

$(document).ready(function () {

    (function () {

        var winWidth = $(window).width(),
            el = $('.header .container');

        $(window).resize(function () {

            if (checkDevice() === 'desktop') {

                el.removeClass('tablet mobile');

                el.addClass('desktop');

                // $('.logo-mobile').removeClass('hidden');

                // $('.logo-desktop').addClass('hidden');
            } else if (checkDevice() !== 'desktop' && checkDevice() === 'mobile') {

                el.removeClass('desktop mobile');

                el.addClass('tablet');

                // $('.logo-mobile').addClass('hidden');

                // $('.logo-desktop').removeClass('hidden');
            } else if (checkDevice() === 'mobile') {

                el.removeClass('tablet desktop');

                el.addClass('mobile');
            }
        });
    })();
    var checkDevice = function checkDevice(options) {
        var result = void 0,
            ww = $(window).width(),
            current = {
            desktop: 1239,
            mobile: 767
        };

        if (options !== 'undefined') {

            for (var item in options) {
                current[item] = item;
            }
        }

        if (ww > current.desktop) {
            result = "desktop";
        } else if (ww < current.desktop && ww > current.mobile) {
            result = "tablet";
        } else if (ww < current.mobile) {
            result = "mobile";
        }

        return result;
    };

    function splitDigits(num) {
        var str = num.toString().split('.');
        if (str[0].length >= 5) {
            str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1 ');
        }
        if (str[1] && str[1].length >= 5) {
            str[1] = str[1].replace(/(\d{3})/g, '$1 ');
        }
        return str.join('.');
    }

    var dimension = checkDevice();

    function completeFillTable(house, tableIndex, dimension) {

        var $tableDesktop = $('.table-desktop[data-table="' + tableIndex + '"]'),
            $tableMobile = $('.table-mobile[data-table="' + tableIndex + '"]');

        if (!house.flats instanceof Array) {

            throw new Error('Flats are not presented as an array');
        } else if (house.flats.length === 0) {

            throw new Error('Flats array is empty');
        }

        if (dimension !== 'mobile') {
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                var _loop = function _loop() {
                    var flat = _step.value;


                    var $view = '';

                    if (flat.viewUrl.length !== 0) {

                        flat.viewUrl.forEach(function (el, index) {

                            $view += '<a class="view-image" ' + (index >= 1 ? 'hidden' : false) + ' href="' + (el.src ? el.src : false) + '" data-fancybox="' + flat.id + '" data-caption="' + (el.name ? el.name : false) + '"><div style="background: url(' + (el.src ? el.src : false) + ') no-repeat center; background-size: cover;"></div></a>';
                        });
                    }

                    $tableDesktop.DataTable().row.add([$view, '' + (flat.rooms !== 0 ? flat.rooms + ' ком.' : '<span hidden>0</span> Студия'), '' + house.houseName, flat.floor, '' + flat.area, '' + flat.price, '' + flat.pricem2]).draw().node();

                    if (flat.discount) {

                        $('tbody tr:last-of-type', $tableDesktop).addClass('has-action');

                        $('tbody tr:last-of-type', $tableDesktop).addClass('flag');
                    }

                    if (flat.furnish) {

                        $('tbody tr:last-of-type', $tableDesktop).addClass('brush');
                    }

                    $('<a class="row-link" href="' + flat.url + '"></a>').appendTo($('tbody tr:last-of-type td:last-of-type', $tableDesktop));
                };

                for (var _iterator = house.flats[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    _loop();
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }
        } else if (dimension === 'mobile') {

            try {
                var _iteratorNormalCompletion2 = true;
                var _didIteratorError2 = false;
                var _iteratorError2 = undefined;

                try {
                    var _loop2 = function _loop2() {
                        var flat = _step2.value;


                        var view = '';

                        if (flat.viewUrl.length !== 0) {

                            flat.viewUrl.forEach(function (el, index) {

                                view += '<a ' + (index >= 1 ? 'hidden' : false) + ' href="' + (el.src ? el.src : false) + '" data-fancybox="' + flat.id + '" data-caption="' + (el.name ? el.name : false) + '"><div style="background: url(' + (el.src ? el.src : false) + ') no-repeat center; background-size: cover;"></div></a>';
                            });
                        }

                        var $flat = '\n\n                    \n\n                    <tr>\n\n                        <td>' + view + '</td>\n\n                        <td>' + (flat.rooms !== 0 ? flat.rooms + ' ком.' : '<span hidden>0</span> Студия') + '</td>\n\n                        <td>' + house.houseName + '</td>\n\n                        <td>' + flat.floor + '</td>\n\n                        <td>' + flat.area + '</td>\n\n                        <td>' + splitDigits(flat.price) + ' \u0440\u0443\u0431.</td>\n\n                        <td class="mobile--hidden"></td>\n\n                        <td class="td-section--mobile td--mobile">\u0421\u0435\u043A\u0446\u0438\u044F</td>\n\n                        <td class="td-floor--mobile td--mobile">\u042D\u0442\u0430\u0436</td>\n\n                        <td class="td-area--mobile td--mobile">\u041F\u043B\u043E\u0449\u0430\u0434\u044C</td>\n\n                        <td class="td-link--mobile"><a href="' + flat.url + '"><div></div></a></td>\n\n                    </tr>\n\n                    ';

                        $tableMobile.find('tbody').append($flat);

                        $('<a class="row-link" href="' + flat.url + '"></a>').appendTo($('tbody tr:last-of-type td:last-of-type', $tableMobile));

                        if (flat.discount) {

                            $('tbody tr:last-of-type', $tableMobile).addClass('has-action');

                            $('tbody tr:last-of-type', $tableMobile).addClass('flag');
                        }

                        if (flat.furnish) {

                            $('tbody tr:last-of-type', $tableMobile).addClass('brush');
                        }
                    };

                    for (var _iterator2 = house.flats[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                        _loop2();
                    }
                } catch (err) {
                    _didIteratorError2 = true;
                    _iteratorError2 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion2 && _iterator2.return) {
                            _iterator2.return();
                        }
                    } finally {
                        if (_didIteratorError2) {
                            throw _iteratorError2;
                        }
                    }
                }
            } catch (err) {

                console.error(err);
            }
        }
    }
    function createTable(house) {
        var index = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'none';
        var dimension = arguments[2];

        var $table = void 0;

        if (dimension !== 'mobile') {

            $table = '\n    \n                <table data-table="' + index + '" class="table table-desktop">\n                    <thead class="thead">\n                        <th data-sort="integer" class="table-th">' + house.houseName + '</th>\n                        <th data-sort="integer" class="table-th" class="th--active">\u0422\u0438\u043F</th>\n                        <th data-sort="integer" class="table-th">\u0421\u0435\u043A\u0446\u0438\u044F</th>\n                        <th data-sort="integer" class="table-th">\u042D\u0442\u0430\u0436</th>\n                        <th data-sort="integer" class="table-th">\u041F\u043B\u043E\u0449\u0430\u0434\u044C</th>\n                        <th data-sort="integer" class="table-th">\u0426\u0435\u043D\u0430</th>\n                        <th data-sort="integer" class="table-th">\u0421\u0442\u043E\u0438\u043C\u043E\u0441\u0442\u044C \u0437\u0430 \u043C<sup>2</sup></th>\n                    </thead>\n                    <tbody></tbody>\n                </table>\n            ';
            // <th class="table-th">Планировка</th>

            if (house.flats.length > 4) {
                $table += '<a href="" role="button" data-table-button="' + index + '" data-step="1" class="show-more show-more--desktop">\u041F\u043E\u043A\u0430\u0437\u0430\u0442\u044C \u0432\u0441\u0435 \u043A\u0432\u0430\u0440\u0442\u0438\u0440\u044B</a>';
            }
        } else {

            $table = '\n    \n                <table data-table="' + index + '" class="table table-mobile">\n                    <thead class="thead">\n                        <th class="table-th">' + house.houseName + '</th>\n                        <th class="table-th" class="th--active">\u0422\u0438\u043F</th>\n                        <th class="table-th">\u0421\u0435\u043A\u0446\u0438\u044F</th>\n                        <th class="table-th">\u042D\u0442\u0430\u0436</th>\n                        <th class="table-th">\u041F\u043B\u043E\u0449\u0430\u0434\u044C</th>\n                        <th class="table-th">\u0426\u0435\u043D\u0430</th>\n                        <th class="table-th">\u0421\u0442\u043E\u0438\u043C\u043E\u0441\u0442\u044C \u0437\u0430 \u043C<sup>2</sup></th>\n                        <th class="table-th table-th-mobile">\u0421\u0432\u043E\u0431\u043E\u0434\u043D\u043E ' + house.total + ' \u043A\u0432.</th>\n                    </thead>\n                    <tbody></tbody>\n                </table>\n            ';

            if (house.flats.length > 4) {
                $table += '<a href="" role="button" data-table-button="' + index + '" data-step="1" class="show-more show-more--mobile">\u041F\u043E\u043A\u0430\u0437\u0430\u0442\u044C \u0432\u0441\u0435 \u043A\u0432\u0430\u0440\u0442\u0438\u0440\u044B</a>';
            }
        }

        return $table;
    }
    function initFillTable(house, table, tableIndex) {
        var $tableDesktop = $('.table-desktop[data-table="' + tableIndex + '"]'),
            $tableMobile = $('.table-mobile[data-table="' + tableIndex + '"]');

        if (house.flats.length === 0) {
            throw new Error('Flats array is empty');
        }

        if (dimension !== 'mobile') {
            //TODO: добавить в flats[0]  .view
            try {
                var _iteratorNormalCompletion3 = true;
                var _didIteratorError3 = false;
                var _iteratorError3 = undefined;

                try {
                    var _loop3 = function _loop3() {
                        var flat = _step3.value;


                        var $view = '';

                        if (flat.viewUrl.length !== 0) {
                            flat.viewUrl.forEach(function (el, index) {
                                $view += '<a data-link="' + flat.src + '" class="view-image" ' + (index >= 1 ? 'hidden' : false) + ' href="' + (el.src ? el.src : false) + '" data-fancybox="' + flat.id + '" data-caption="' + (el.name ? el.name : false) + '"><div style="background: url(' + (el.src ? el.src : false) + ') no-repeat center; background-size: cover;"></div></a>';
                            });
                        }

                        $tableDesktop.DataTable().row.add([$view, '' + (flat.rooms !== 0 ? flat.rooms + ' ком.' : '<span hidden>0</span> Студия'), '' + house.houseName, flat.floor, '' + flat.area, '' + flat.price, '' + flat.pricem2]).draw().node();
                        if (flat.discount) {
                            $('tbody tr:last-of-type', $tableDesktop).addClass('has-action');
                            $('tbody tr:last-of-type', $tableDesktop).addClass('flag');
                        }
                        if (flat.furnish) {
                            $('tbody tr:last-of-type', $tableDesktop).addClass('brush');
                        }
                        $('<a class="row-link" href="' + flat.url + '"></a>').appendTo($('tbody tr:last-of-type td:last-of-type', $tableDesktop));
                    };

                    for (var _iterator3 = house.flats[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                        _loop3();
                    }
                } catch (err) {
                    _didIteratorError3 = true;
                    _iteratorError3 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion3 && _iterator3.return) {
                            _iterator3.return();
                        }
                    } finally {
                        if (_didIteratorError3) {
                            throw _iteratorError3;
                        }
                    }
                }
            } catch (err) {
                console.error(err);
            }
        } else {

            try {
                var _iteratorNormalCompletion4 = true;
                var _didIteratorError4 = false;
                var _iteratorError4 = undefined;

                try {
                    var _loop4 = function _loop4() {
                        var flat = _step4.value;


                        var $flat = void 0,
                            $view = void 0;

                        $flat = '\n    \n                    <tr>\n                        <td>';

                        if (flat.viewUrl.length !== 0) {
                            flat.viewUrl.forEach(function (el, index) {
                                $flat += '<a ' + (index >= 1 ? 'hidden' : false) + ' href="' + (el.src ? el.src : false) + '" data-fancybox="' + flat.id + '" data-caption="' + (el.name ? el.name : false) + '"><div style="background: url(' + (el.src ? el.src : false) + ') no-repeat center; background-size: cover;"></div></a>';
                            });
                        }

                        $flat += '</td>\n                        <td>' + (flat.rooms !== 0 ? flat.rooms + ' ком.' : '<span hidden>0</span> Студия') + '</td>\n                        <td>' + house.houseName + '</td>\n                        <td>' + flat.floor + '</td>\n                        <td>' + flat.area + ' \u043C<sup>2</sup></td>\n                        <td>' + splitDigits(flat.price) + ' \u0440\u0443\u0431.</td>\n                        <td class="mobile--hidden"></td>\n                        <td class="td-section--mobile td--mobile">\u0421\u0435\u043A\u0446\u0438\u044F</td>\n                        <td class="td-floor--mobile td--mobile">\u042D\u0442\u0430\u0436</td>\n                        <td class="td-area--mobile td--mobile">\u041F\u043B\u043E\u0449\u0430\u0434\u044C</td>\n                        <td class="td-link--mobile"><a href="' + flat.url + '"><div></div></a></td>\n                    </tr>\n                    ';
                        $tableMobile.find('tbody').append($flat);
                        $('<a class="row-link" href="' + flat.url + '"></a>').appendTo($('tbody tr:last-of-type td:last-of-type', $tableMobile));
                        if (flat.discount) {
                            $('tbody tr:last-of-type', $tableMobile).addClass('has-action');
                            $('tbody tr:last-of-type', $tableMobile).addClass('flag');
                        }
                        if (flat.furnish) {
                            $('tbody tr:last-of-type', $tableMobile).addClass('brush');
                        }
                    };

                    for (var _iterator4 = house.flats[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                        _loop4();
                    }
                } catch (err) {
                    _didIteratorError4 = true;
                    _iteratorError4 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion4 && _iterator4.return) {
                            _iterator4.return();
                        }
                    } finally {
                        if (_didIteratorError4) {
                            throw _iteratorError4;
                        }
                    }
                }
            } catch (err) {
                console.error(err);
            }
        }
    }
    function insertTable(table, wrapper) {
        var $table = $(table),
            $wrapper = wrapper;

        if (!$table) {
            throw new Error("You haven't specified table or table doesn't exist");
        } else if (!$wrapper) {
            throw new Error("You haven't specified wrapper or wrapper doesn't exist");
        }

        $table.appendTo($wrapper);
    }
    function splitDigits(num) {
        var str = num.toString().split('.');
        if (str[0].length >= 5) {
            str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1 ');
        }
        if (str[1] && str[1].length >= 5) {
            str[1] = str[1].replace(/(\d{3})/g, '$1 ');
        }
        return str.join('.');
    }

    function makeTableSortable(table) {
        var $table = $(table);

        if (!$($table).hasClass('sorting-inited')) {

            try {
                var _$$DataTable;

                $($table).DataTable((_$$DataTable = {
                    'searching': false,
                    'bPaginate': false,
                    'bLengthChange': false,
                    'bInfo': false,
                    "paging": false
                }, _defineProperty(_$$DataTable, 'searching', false), _defineProperty(_$$DataTable, 'createdRow', function createdRow(row, data, dataIndex) {

                    $(row).find('td').eq(4).html(data[4] + ' м<sup>2</sup>');
                    $(row).find('td').eq(5).html(splitDigits(parseInt(data[5], 10)) + ' руб.');
                    $(row).find('td').eq(6).html(splitDigits(parseInt(data[6], 10)) + ' руб.');
                    // $(row).find('td').last().append('<a href="#">fff</a>')
                }), _$$DataTable));

                $($table).addClass('sorting-inited');
                $($table).find('thead th').click(function () {
                    $(this).siblings().removeClass('th--active');
                    $(this).toggleClass('th--active');
                });
            } catch (err) {
                console.error(err);
            }
        } else {
            throw new Error('This table is already sortable');
        }
    }

    (function () {
        var sliderWrappers = [$('.sidebar__slider-1'), $('.sidebar__slider-2'), $('.sidebar__slider-3'), $('.sidebar__slider-4')],
            sliderRangeNumWrappers = [$('.sidebar__slider-1__values'), $('.sidebar__slider-2__values'), $('.sidebar__slider-3__values'), $('.sidebar__slider-4__values')];

        var sliderStartValues = [{
            min: sliderWrappers[0].data('min'),
            max: sliderWrappers[0].data('max')
        }, {
            min: sliderWrappers[1].data('min'),
            max: sliderWrappers[1].data('max')
        }, {
            min: sliderWrappers[2].data('min'),
            max: sliderWrappers[2].data('max')
        }, {
            min: sliderWrappers[3].data('min'),
            max: sliderWrappers[3].data('max')
        }];

        var rangeValues = [[sliderStartValues[0].min, parseInt(((sliderStartValues[0].min + sliderStartValues[0].max) / 2 + sliderStartValues[0].min) / 2, 10), parseInt((sliderStartValues[0].min + sliderStartValues[0].max) / 2, 10), parseInt(((sliderStartValues[0].min + sliderStartValues[0].max) / 2 + sliderStartValues[0].max) / 2, 10), sliderStartValues[0].max], [sliderStartValues[1].min, parseInt(((sliderStartValues[1].min + sliderStartValues[1].max) / 2 + sliderStartValues[1].min) / 2, 10), parseInt((sliderStartValues[1].min + sliderStartValues[1].max) / 2, 10), parseInt(((sliderStartValues[1].min + sliderStartValues[1].max) / 2 + sliderStartValues[1].max) / 2, 10), sliderStartValues[1].max], [sliderStartValues[2].min, parseInt(((sliderStartValues[2].min + sliderStartValues[2].max) / 2 + sliderStartValues[2].min) / 2, 10), parseInt((sliderStartValues[2].min + sliderStartValues[2].max) / 2, 10), parseInt(((sliderStartValues[2].min + sliderStartValues[2].max) / 2 + sliderStartValues[2].max) / 2, 10), sliderStartValues[2].max], [sliderStartValues[3].min, parseInt(((sliderStartValues[3].min + sliderStartValues[3].max) / 2 + sliderStartValues[3].min) / 2, 10), parseInt((sliderStartValues[3].min + sliderStartValues[3].max) / 2, 10), parseInt(((sliderStartValues[3].min + sliderStartValues[3].max) / 2 + sliderStartValues[3].max) / 2, 10), sliderStartValues[3].max]];

        if (sliderWrappers[0] && rangeValues[0][0]) {
            try {
                sliderRangeNumWrappers.forEach(function (el, index) {

                    for (var i = 0; i < 5; i++) {
                        var small = document.createElement('small');
                        small.innerHTML = rangeValues[index][i] + '';
                        el.append(small);
                    }
                });
            } catch (err) {
                console.error(err);
            }
        }
    })();

    // Initialization of sliders in sidebar
    (function () {

        var one = $('#slider-1'),
            two = $('#slider-2'),
            three = $('#slider-3'),
            four = $('#slider-4');

        if (one) {

            var val = [$('.sidebar__slider-1').data('min'), $('.sidebar__slider-1').data('max')];

            $('[name="price-from"]').val(val[0]);
            $('[name="price-to"]').val(val[1]);

            one.slider({
                min: val[0],
                max: val[1],
                range: true,
                values: [val[0], val[1]],
                slide: function slide(event, ui) {
                    $('.sidebar__slider-1__inputs input:first-of-type').val(ui.values[0]);
                    $('.sidebar__slider-1__inputs input:last-of-type').val(ui.values[1]);
                },
                stop: function stop(event, ui) {
                    getFlats();
                }
            });
        }

        if (two) {

            var _val = [$('.sidebar__slider-2').data('min'), $('.sidebar__slider-2').data('max')];

            $('[name="price-meter-from"]').val(_val[0]);
            $('[name="price-meter-to"]').val(_val[1]);

            two.slider({
                min: _val[0],
                max: _val[1],
                range: true,
                values: [_val[0], _val[1]],
                slide: function slide(event, ui) {
                    $('.sidebar__slider-2__inputs input:first-of-type').val(ui.values[0]);
                    $('.sidebar__slider-2__inputs input:last-of-type').val(ui.values[1]);
                },
                stop: function stop() {
                    getFlats();
                }
            });
        }

        if (three) {

            var _val2 = [$('.sidebar__slider-3').data('min'), $('.sidebar__slider-3').data('max')];

            $('[name="floor-from"]').val(_val2[0]);
            $('[name="floor-to"]').val(_val2[1]);

            three.slider({
                min: _val2[0],
                max: _val2[1],
                range: true,
                values: [_val2[0], _val2[1]],
                slide: function slide(event, ui) {
                    $('.sidebar__slider-3__inputs input:first-of-type').val(ui.values[0]);
                    $('.sidebar__slider-3__inputs input:last-of-type').val(ui.values[1]);
                },
                stop: function stop() {
                    getFlats();
                }
            });
        }

        if (four) {

            var _val3 = [$('.sidebar__slider-4').data('min'), $('.sidebar__slider-4').data('max')];

            $('[name="sq-from"]').val(_val3[0]);
            $('[name="sq-to"]').val(_val3[1]);

            four.slider({
                min: _val3[0],
                max: _val3[1],
                range: true,
                values: [_val3[0], _val3[1]],
                slide: function slide(event, ui) {
                    $('.sidebar__slider-4__inputs input:first-of-type').val(ui.values[0]);
                    $('.sidebar__slider-4__inputs input:last-of-type').val(ui.values[1]);
                },
                stop: function stop() {
                    getFlats();
                }
            });
        }

        $('aside.sidebar-filter input').on('change', function () {
            getFlats();
            one.slider({ values: [$('[name="price-from"]').val(), $('[name="price-to"]').val()] });
            two.slider({ values: [$('[name="price-meter-from"]').val(), $('[name="price-meter-to"]').val()] });
            three.slider({ values: [$('[name="floor-from"]').val(), $('[name="floor-to"]').val()] });
            four.slider({ values: [$('[name="sq-from"]').val(), $('[name="sq-to"]').val()] });
        });
    })();

    // LIGHTBOX—————————————————————————
    function fancyboxDefaults() {

        var elems = $("[data-fancybox]");

        $.extend($.fancybox.defaults, {
            animationEffect: "zoom-in-out",
            touch: false,
            idleTime: 0,
            margin: [40, 40],
            buttons: ['close'],
            afterLoad: function afterLoad() {
                var imgWrapper = $('.fancybox-container .fancybox-image-wrap'),
                    el = $('.fancybox-container .fancybox-caption-wrap');

                imgWrapper.prepend(el);
            },
            afterShow: function afterShow() {
                var imgWrapper = $('.fancybox-container .fancybox-image-wrap'),
                    el = $('.fancybox-container .fancybox-navigation');

                el.addClass('fancybox-navigation-visible');
                $('.fancybox-container .fancybox-caption-wrap').addClass('fancybox-caption-wrap--show');
                $('.fancybox-container .fancybox-navigation .count-wrapper').remove();
                $('.fancybox-container .fancybox-navigation').prepend('<div class="count-wrapper"><span class="image-index">' + (this.index + 1) + '</span>/<span class="image-total"> / ' + $('.fancybox-image').length + '</span></div>');
            },
            beforeClose: function beforeClose() {
                $('.fancybox-navigation-visible').removeClass('fancybox-navigation-visible');
            }
        });
    }
    //——————————————————————————————


    // TABLE SORTING———————————————————————

    function moreFlats(houseNum, offset) {

        function getRooms() {
            var arr = [];

            $('aside.sidebar #check-1').is(':checked') ? arr.push(0) : false;
            $('aside.sidebar #check-2').is(':checked') ? arr.push(1) : false;
            $('aside.sidebar #check-3').is(':checked') ? arr.push(2) : false;
            $('aside.sidebar #check-4').is(':checked') ? arr.push(3) : false;

            return arr;
        }

        var house = void 0,
            offsetReady = offset,
            loader = $('.loader'),
            foundSomething = false,
            $tablesWrapper = $('.filter .container'),
            opts = {
            get_flats: 'json',
            limit: 10,
            offset: offsetReady,
            house: houseNum,
            priceFrom: $('aside.sidebar [name="price-from"]').val(),
            priceTo: $('aside.sidebar [name="price-to"]').val(),
            priceMeterFrom: $('aside.sidebar [name="price-meter-from"]').val(),
            priceMeterTo: $('aside.sidebar [name="price-meter-to"]').val(),
            floorFrom: $('aside.sidebar [name="floor-from"]').val(),
            floorTo: $('aside.sidebar [name="floor-to"]').val(),
            rooms: getRooms(), //массив
            sqFrom: $('aside.sidebar [name="sq-from"]').val(),
            sqTo: $('aside.sidebar [name="sq-to"]').val(),
            furnish: '' + ($('aside.sidebar #checkbox-1').is(':checked') ? '1' : '0'),
            flatview: '' + ($('aside.sidebar #checkbox-2').is(':checked') ? '1' : '0'),
            discount: '' + ($('aside.sidebar #checkbox-3').is(':checked') ? '1' : '0')
        };

        $.ajax({
            method: "GET",
            url: '/choose/search/',
            data: opts,
            dataType: 'json',
            beforeSend: function beforeSend() {
                loader.addClass('loader--active');
            }
        }).done(function (response) {
            console.log(response);
            try {
                house = response.houses[0];
            } catch (err) {
                console.error(err);
            }

            if (house.flats.length === 0) {
                $('.show-more[data-table-button="' + (houseNum - 1) + '"]').remove();
                loader.removeClass('loader--active');
                return;
            }

            completeFillTable(house, houseNum - 1, dimension);
            loader.removeClass('loader--active');

            if (house.flats.length < 10) {
                $('.show-more[data-table-button="' + (houseNum - 1) + '"]').remove();
            }

            fancyboxDefaults();
        });
    }

    function getFlats() {

        var houses = [],
            foundSomething = false,
            $tablesWrapper = $('.filter .container'),
            loader = $('.loader');

        function getRooms() {
            var arr = [];

            $('aside.sidebar #check-1').is(':checked') ? arr.push(0) : false;
            $('aside.sidebar #check-2').is(':checked') ? arr.push(1) : false;
            $('aside.sidebar #check-3').is(':checked') ? arr.push(2) : false;
            $('aside.sidebar #check-4').is(':checked') ? arr.push(3) : false;

            return arr;
        }

        var sidebar = $("aside.sidebar"),
            opts = {
            get_flats: 'json',
            priceFrom: $('aside.sidebar [name="price-from"]').val(),
            priceTo: $('aside.sidebar [name="price-to"]').val(),
            priceMeterFrom: $('aside.sidebar [name="price-meter-from"]').val(),
            priceMeterTo: $('aside.sidebar [name="price-meter-to"]').val(),
            floorFrom: $('aside.sidebar [name="floor-from"]').val(),
            floorTo: $('aside.sidebar [name="floor-to"]').val(),
            rooms: getRooms(), //массив
            sqFrom: $('aside.sidebar [name="sq-from"]').val(),
            sqTo: $('aside.sidebar [name="sq-to"]').val(),
            furnish: '' + ($('aside.sidebar #checkbox-1').is(':checked') ? '1' : '0'),
            flatview: '' + ($('aside.sidebar #checkbox-2').is(':checked') ? '1' : '0'),
            discount: '' + ($('aside.sidebar #checkbox-3').is(':checked') ? '1' : '0')
        };

        $.ajax({
            method: "GET",
            url: '/choose/search/',
            data: opts,
            dataType: 'json',
            beforeSend: function beforeSend() {
                $tablesWrapper.empty();
                $tablesWrapper.prepend('<h2>Поиск квартир</h2>');
                loader.addClass('loader--active');
            }
        }).done(function (response) {
            console.log(response);
            try {
                houses = response.houses;

                var _iteratorNormalCompletion5 = true;
                var _didIteratorError5 = false;
                var _iteratorError5 = undefined;

                try {
                    for (var _iterator5 = houses[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
                        var house = _step5.value;

                        if (house.flats.length !== 0) foundSomething = true;
                    }
                } catch (err) {
                    _didIteratorError5 = true;
                    _iteratorError5 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion5 && _iterator5.return) {
                            _iterator5.return();
                        }
                    } finally {
                        if (_didIteratorError5) {
                            throw _iteratorError5;
                        }
                    }
                }
            } catch (err) {
                console.error(err);
            }

            if (foundSomething) {
                $tablesWrapper.empty();
                $tablesWrapper.prepend('<h2>Поиск квартир</h2>');
            } else {
                $tablesWrapper.empty();
                $tablesWrapper.prepend('<h2 class="h2-no-result">Квартир не найдено. <br/>Измените параметры <br/>поиска.</h2>');
                loader.removeClass('loader--active');
            }
            //Создание и вставка таблиц в DOM
            houses.forEach(function (item, index) {
                if (item.flats.length === 0) return;

                var $table = createTable(item, index, dimension); // Создание таблицы

                insertTable($table, $tablesWrapper); // Вставка таблицы в DOM
                makeTableSortable($('.table-desktop[data-table="' + index + '"]')); // Инициализация сортируемости таблицы
                initFillTable(item, $('.table-desktop[data-table="' + index + '"]'), index);
                loader.removeClass('loader--active');

                $('.show-more[data-table-button="' + index + '"]').unbind('click').click(function (event) {
                    var offset = $('.table[data-table="' + index + '"] tbody tr').get().length;
                    event.preventDefault();
                    moreFlats(index + 1, offset);
                    offset += 10;
                });
            });
            $('.row-link').click(function () {
                var lastSeenSection = $(this).closest('table').data('table');

                store.set('lastSeenSection', +lastSeenSection);
            });
            if (store.get('lastSeenSection') !== null) {
                $('html').animate({
                    'scrollTop': $('table.table[data-table="' + store.get('lastSeenSection') + '"]').offset().top + 'px'
                }, 350, function () {
                    store.remove('lastSeenSection');
                });
            }
            fancyboxDefaults();
        }).fail(function (err) {
            throw err;
        });
    }

    function styleScrollbar() {
        var elem = $('.sidebar .sidebar__wrapper');
        var elemTable = $('.sidebar .table-wrapper');

        if ($(window).width() < 1023) {
            elem.niceScroll({
                cursorcolor: "#F8B00A",
                cursorfixedheight: 0,
                cursoropacitymin: 0,
                cursorwidth: 0,
                preventmultitouchscrolling: false,
                touchbehavior: true,
                horizrailenabled: true,
                cursorborder: '0px solid transparent'
            });

            elemTable.niceScroll({
                cursorcolor: "#F8B00A",
                cursorfixedheight: 60,
                cursoropacitymin: 1,
                cursorborder: '0px solid transparent',
                preventmultitouchscrolling: false,
                touchbehavior: true,
                horizrailenabled: true,
                railoffset: {
                    left: 23,
                    top: 0
                }
            });
        } else {
            elem.niceScroll({
                cursorcolor: "#F8B00A",
                cursorfixedheight: 0,
                cursoropacitymin: 0,
                cursorwidth: 0,
                preventmultitouchscrolling: false,
                horizrailenabled: true,
                cursorborder: '0px solid transparent'
            });

            elemTable.niceScroll({
                cursorcolor: "#F8B00A",
                cursorfixedheight: 60,
                cursoropacitymin: 1,
                cursorborder: '0px solid transparent',
                horizrailenabled: true,
                preventmultitouchscrolling: false,
                railoffset: {
                    left: 23,
                    top: 0
                }
            });
        }
    }
    styleScrollbar();

    (function () {
        $('.sidebar-trigger').on('click', function () {
            var $trigger = $(this),
                $sidebar = $('aside.sidebar'),
                $filter = $('.filter'),
                $footer = $('.footer'),
                $loader = $('.loader');

            $sidebar.toggleClass('sidebar--active');
            $filter.toggleClass('sidebar-filter--active');
            $footer.toggleClass('sidebar-filter--active');
            $loader.toggleClass('loader-filter--active');
        });
    })();

    (function () {
        var _loop5 = function _loop5(i, len) {

            $('.sidebar__slider-' + i + '__inputs input').on('change', function () {

                var val = [],
                    checkboxOne = $('.sidebar__slider-' + i + '__inputs input:nth-of-type(1)'),
                    checkboxTwo = $('.sidebar__slider-' + i + '__inputs input:nth-of-type(2)');

                if ($(this).prop('name') == checkboxOne.prop('name')) {

                    val.push($(this).val());

                    val.push(checkboxTwo.val());
                } else if ($(this).prop("name") == checkboxTwo.prop('name')) {

                    val.push(checkboxOne.val());

                    val.push($(this).val());
                }

                store.set('slider-' + i, val);
            });

            $('#slider-' + i).on('slidestop', function (event, ui) {

                var val = ui.values;

                store.set('slider-' + i, val);
            });
        };

        // Sliderrs

        for (var i = 1, len = $('[class*="sidebar__slider-"][data-min][data-max]').length; i <= len; i++) {
            _loop5(i, len);
        }

        // Rooms

        var _loop6 = function _loop6(i, len) {

            $('#check-' + i).on('change', function () {

                var val = $(this).is(':checked');

                store.set('room-' + (i - 1), val);
            });
        };

        for (var i = 1, len = $('.sidebar__room__check input').length; i <= len; i++) {
            _loop6(i, len);
        }

        // Options

        var _loop7 = function _loop7(i, len) {

            $('#checkbox-' + i).on('change', function () {

                var val = $(this).is(':checked');

                store.set('option-' + i, val);
            });
        };

        for (var i = 1, len = $('.sidebar__option input').length; i <= len; i++) {
            _loop7(i, len);
        }
    })();

    // Apply old filter preferences
    function applyFilterPref() {

        try {

            // Sliderrs
            for (var i = 1, len = $('[class*="sidebar__slider-"][data-min][data-max]').length; i <= len; i++) {

                var initialValue = store.get('slider-' + i);

                if (store.get('slider-' + i) !== null) {

                    $('#slider-' + i).slider({ values: initialValue });
                    $('.sidebar__slider-' + i + ' .sidebar__slider-' + i + '__inputs input:first-of-type').val(initialValue[0]);
                    $('.sidebar__slider-' + i + ' .sidebar__slider-' + i + '__inputs input:last-of-type').val(initialValue[1]);
                }
            }

            // Rooms
            for (var _i = 1, _len = $('.sidebar__room__check input').length; _i <= _len; _i++) {

                if (store.get('room-' + (_i - 1)) !== null) {
                    if (store.get('room-' + (_i - 1)) === true) {
                        $('#check-' + _i).prop('checked', true);
                    } else {
                        $('#check-' + _i).prop('checked', false);
                    }
                }
            }

            // Options
            for (var _i2 = 1, _len2 = $('.sidebar__option input').length; _i2 <= _len2; _i2++) {

                if (store.get('option-' + _i2) !== null) {
                    if (store.get('option-' + _i2) === true) {
                        $('#checkbox-' + _i2).prop('checked', true);
                    } else {
                        $('#checkbox-' + _i2).prop('checked', false);
                    }
                }
            }
        } catch (e) {
            console.error(e);
        }
    }

    // INIT ———————————————————————
    (function () {
        applyFilterPref();
        getFlats();
    })();
    // ————————————————————————————
});