"use strict";

$(document).ready(function () {

    var checkDevice = function checkDevice(options) {

        var result = void 0,
            ww = $(window).width(),
            current = {

            desktop: 1239,

            mobile: 767

        };

        if (options !== 'undefined') {

            for (var item in options) {

                current[item] = item;
            }
        }

        if (ww > current.desktop) {

            result = "desktop";
        } else if (ww < current.desktop && ww > current.mobile) {

            result = "tablet";
        } else if (ww < current.mobile) {

            result = "mobile";
        }

        return result;
    };

    // COMMON ———————————————————————————————————————————————————————————————


    $('[data-src="#more-works"]').fancybox({
        smallBtn: false,
        touch: false,
        buttons: [],
        beforeShow: function beforeShow() {
            $('.more-works').addClass('more-works--active');
            $('.fancybox-slide').addClass('more-works-slide--active');
            $('.fancybox-container').addClass('fancybox-container--active');
        },
        afterClose: function afterClose() {
            $('.more-works').removeClass('more-works--active');
        },
        onActivate: function onActivate() {
            $('.fancybox-stage').css('pointerEvents', 'all');
            $('.fancybox-toolbar').hide();
        }
    });

    // HEADER —————————————————————————————————————————————————————————————————————————————————

    // Navigation drop

    (function () {

        if ($('.nav-dropped')) {
            try {

                var $initialElem = $('.nav-dropped'),
                    $otherElems = $('.sub-trigger'),
                    $background = $('.sub-menu--background'),
                    visibleByDefault = false,
                    state = false;

                if (document.querySelector('.sub-trigger.nav-dropped')) {
                    $background.addClass('active');
                    visibleByDefault = true;
                }

                $otherElems.on('mouseover', function () {

                    if ($(this).hasClass('nav-dropped')) {
                        return;
                    } else {
                        $('.nav-dropped').removeClass('nav-dropped');
                        $(this).addClass('nav-dropped');
                        $background.addClass('active');
                    }
                });

                $('.header .sub-menu').on('mouseover', function () {
                    if (!visibleByDefault) {
                        $background.addClass('active');
                    }
                });

                $('.nav__wrapper .sub-menu').on('mouseover', function () {
                    state = false;
                    $initialElem.removeClass('nav-dropped');
                });

                $('.nav__wrapper .sub-menu').on('mouseout', function () {
                    state = true;
                });

                $('.nav__wrapper ul').on('mouseout', function () {
                    if (state) {

                        $otherElems.removeClass('nav-dropped');
                        $initialElem.addClass('nav-dropped');

                        if (!visibleByDefault) {
                            $background.removeClass('active');
                        }
                    }
                });
            } catch (err) {
                console.error(err);
            }
        }
    })();

    // headerClass by width of device
    (function () {

        var winWidth = $(window).width(),
            el = $('.header .container');

        $(window).resize(function () {

            if (checkDevice() === 'desktop') {
                el.removeClass('tablet mobile');
                el.addClass('desktop');
                // $('.logo-mobile').removeClass('hidden');
                // $('.logo-desktop').addClass('hidden');
            } else if (checkDevice() !== 'desktop' && checkDevice() === 'mobile') {
                el.removeClass('desktop mobile');
                el.addClass('tablet');
                // $('.logo-mobile').addClass('hidden');
                // $('.logo-desktop').removeClass('hidden');
            } else if (checkDevice() === 'mobile') {
                el.removeClass('tablet desktop');
                el.addClass('mobile');
            }
        });
    })();

    //toggle submenu
    // ++++++++++

    $('nav .nav__wrapper > ul > li > a').on('click', function (e) {
        var sub = $('nav .sub-menu'),
            thisSub = $(this).parent().find('.sub-menu'),
            subHeight = $(this).siblings('.sub-menu').height(),
            subHeightCollapsed = 38;

        if ($(thisSub).length === 0) {
            return;
        }

        if ($(window).width() < 1240) {

            e.preventDefault();

            // $('.nav .nav__wrapper > ul > li').parent().css({
            //     'height': '38px'
            // });

            if (!thisSub.hasClass('submenu--on')) {
                sub.removeClass('submenu--on');
                thisSub.addClass('submenu--on');

                sub.parent().css({
                    'height': subHeightCollapsed + 'px'
                });

                $(this).parent().css({
                    'height': subHeight + subHeightCollapsed + 'px'
                });
            } else if (thisSub.hasClass('submenu--on')) {

                thisSub.removeClass('submenu--on');
                $(this).parent().css({
                    'height': subHeightCollapsed + 'px'
                });
            }
        }
    });

    //toggle menu

    $('.header .hamburger').on('click', function (e) {
        e.stopPropagation();

        $(this).toggleClass('hamburger--active');
        $('.header nav').toggleClass('menu--mobile--on');
        $('nav .nav__wrapper .sub-menu').removeClass('submenu--on');
        $('nav .nav__wrapper .sub-menu').parent().css({
            'height': '38px'
        });
        $('html, body').toggleClass('noscroll');
    });

    // FOOTER —————————————————————————————————————————————————————————————————

    var isFullscreen = $('body').hasClass('page--fullscreen');

    if (isFullscreen) {

        var footer = $('.footer');
        $('body').mousewheel(function (event) {
            if (checkDevice() === 'desktop') {
                if (event.deltaY < 0) {
                    footer.css({
                        'transform': 'translate3d(0,-' + footer.height() + 'px,0)'
                    });
                } else if (event.deltaY > 0) {
                    footer.css({
                        'transform': 'translate3d(0,0,0)'
                    });
                }
            }
        });

        if (checkDevice() === 'tablet') {

            if ($('body').hasClass('page--fullscreen')) {

                $('body, html').on('swipeup', function () {
                    footer.css({
                        'transform': 'translate3d(0,-' + footer.height() + 'px,0)'
                    });
                });

                $('body, html').on('swipedown', function () {
                    footer.css({
                        'transform': 'translate3d(0,0,0)'
                    });
                });
            }
        }

        $(window).resize(function () {

            footer.css({
                'transform': 'translate3d(0,0,0)'
            });
        });

        (function () {
            var footer = $('footer.footer');

            footer.css({
                'bottom': '-' + $('footer.footer').height() + 'px'
            });

            $(window).resize(function () {
                footer.css({
                    'bottom': '-' + $('footer.footer').height() + 'px'
                });
            });
        })();

        (function () {
            var el = $('.header ~ [class*="content"]');

            if ($('body').hasClass('page--fullscreen')) {

                if (checkDevice() === 'mobile') {
                    $('body, html').on('swipeup', function () {
                        el.css({
                            'transform': 'translateY(-100vh)'
                        });
                    });

                    $('body, html').on('swipedown', function () {
                        el.css({
                            'transform': 'translateY(0vh)'
                        });
                    });
                }
            }
        })();

        if ($('.owl-carousel-under')) {
            $('.owl-carousel-under').owlCarousel({
                dots: false,
                nav: true,
                items: 1,
                navText: false,
                touchDrag: true,
                mouseDrag: true,
                loop: true
            });
        }

        $('.fullscreen-mobile--hidden').removeClass('fullscreen-mobile--hidden');
    }

    (function () {

        if ($('section.filter').length === 0 && $('.flat-mode').length === 0) {
            localStorage.clear();
        }
    })();
});