'use strict';

$(document).ready(function () {

    (function () {

        var winWidth = $(window).width(),
            el = $('.header .container');

        $(window).resize(function () {

            if (checkDevice() === 'desktop') {

                el.removeClass('tablet mobile');

                el.addClass('desktop');

                // $('.logo-mobile').removeClass('hidden');


                // $('.logo-desktop').addClass('hidden');

            } else if (checkDevice() !== 'desktop' && checkDevice() === 'mobile') {

                el.removeClass('desktop mobile');

                el.addClass('tablet');

                // $('.logo-mobile').addClass('hidden');


                // $('.logo-desktop').removeClass('hidden');

            } else if (checkDevice() === 'mobile') {

                el.removeClass('tablet desktop');

                el.addClass('mobile');
            }
        });
    })();
    var checkDevice = function checkDevice(options) {
        var result = void 0,
            ww = $(window).width(),
            current = {
            desktop: 1239,
            mobile: 767
        };

        if (options !== 'undefined') {

            for (var item in options) {
                current[item] = item;
            }
        }

        if (ww > current.desktop) {
            result = "desktop";
        } else if (ww < current.desktop && ww > current.mobile) {
            result = "tablet";
        } else if (ww < current.mobile) {
            result = "mobile";
        }

        return result;
    };

    function splitDigits(num) {
        var str = num.toString().split('.');
        if (str[0].length >= 5) {
            str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1 ');
        }
        if (str[1] && str[1].length >= 5) {
            str[1] = str[1].replace(/(\d{3})/g, '$1 ');
        }
        return str.join('.');
    }

    var dimension = checkDevice();

    (function () {

        var flat = $('.flats .flats__container__grid__wrapper__icon:not(.flat--sold):not(.flat--unactive)'),
            popup = $('.flats__popup');

        flat.on('click', function () {

            var flatId = $(this).data('id');

            $.ajax({

                method: "GET",

                url: '/choose/',

                data: { flatId: flatId },

                dataType: 'json'

            }).done(function (data) {

                var parsedData = data;

                var $view = '';

                if (parsedData.viewUrl.length !== 0) {

                    parsedData.viewUrl.forEach(function (el, index) {

                        $view += '<a ' + (index >= 1 ? 'style="display: none;"' : false) + ' href="' + (el.src ? el.src : false) + '" data-fancybox="' + flat.id + '" data-caption="' + (el.name ? el.name : false) + '" class="flats__view"><div style="background: url(' + (el.src ? el.src : false) + ') no-repeat center; background-size: cover;"><p>\u0412\u0438\u0434 \u0438\u0437 \u043E\u043A\u043D\u0430</p></div></a>';
                    });
                }

                // history.pushState({}, 'Выбор квартиры', parsedData.url)


                var filledInputsData = {},
                    inputForFilling = $('#form-flat input[name="flat"]'),
                    content = '\n\n              <div class="flats__popup--close"></div>\n\n    \n\n              <div class="flats__popup__container__left">\n\n    \n\n                <div class="flats__plan-1">\n\n                  <img src="' + (parsedData.planImg ? parsedData.planImg : '') + '" alt="">\n\n                </div>\n\n    \n\n                ' + (parsedData.planSectionImg ? '<div class="flats__plan-2"><small>' + (parsedData.house ? parsedData.house : '') + '</small><img src="' + parsedData.planSectionImg + '" alt=""></div>' : '') + '\n\n                \n\n                ' + (parsedData.planFloorImg ? '<div class="flats__plan-3"><small>\u042D\u0442\u0430\u0436</small><img src="' + (parsedData.planFloorImg ? parsedData.planFloorImg : '') + '" alt=""></div>' : '') + '\n\n    \n\n                <ul>\n\n                  <li>* \u0426\u0435\u043D\u0430 \u0434\u0430\u043D\u0430 \u0441 \u0443\u0447\u0435\u0442\u043E\u043C \u0441\u043A\u0438\u0434\u043E\u043A</li>\n\n                  <li>* \u0420\u0435\u0437\u0435\u0440\u0432 \u043D\u0435 \u0444\u0438\u043A\u0441\u0438\u0440\u0443\u0435\u0442 \u0441\u0442\u043E\u0438\u043C\u043E\u0441\u0442\u044C \u0438 \u0443\u0441\u043B\u043E\u0432\u0438\u044F \u043E\u043F\u043B\u0430\u0442\u044B</li>\n\n                  <li>* \u0426\u0435\u043D\u0430 \u0434\u0435\u0439\u0441\u0442\u0432\u0438\u0442\u0435\u043B\u044C\u043D\u0430 \u043F\u0440\u0438 100% \u043E\u043F\u043B\u0430\u0442\u0435</li>\n\n                </ul>\n\n    \n\n              </div>\n\n              <div class="flats__popup__container__right ' + (parsedData.sectionName && parsedData.section ? 'has-section-name' : '') + ' ' + (parsedData.viewUrl.length !== 0 ? 'has-image' : '') + '">\n\n                <h3>\u041A\u0432\u0430\u0440\u0442\u0438\u0440\u0430 ' + (parsedData.num ? parsedData.num : '') + '</h3>\n\n                ' + (parsedData.sectionName && parsedData.section ? '<h4>\u0421\u0435\u043A\u0446\u0438\u044F \u2116' + parsedData.section + ' \xAB' + parsedData.sectionName + '\xBB</h4>' : '') + '\n\n                <div class="flats__flat-info">\n\n                  <div class="' + (parsedData.floor ? '' : 'hidden') + '">\u042D\u0442\u0430\u0436:<br/><span>' + (parsedData.floor ? parsedData.floor : '') + '</span></div>\n\n                 <div class="' + (parsedData.rooms ? '' : 'hidden') + '">\u041A\u043E\u043C\u043D\u0430\u0442:<br/><span>' + (parsedData.rooms ? parsedData.rooms : '') + (parsedData.rooms == 0 ? 'Студия' : '') + '</span></div>\n\n                 <div class="' + (parsedData.area ? '' : 'hidden') + '">\u041F\u043B\u043E\u0449\u0430\u0434\u044C:<br/><span>' + (parsedData.area ? parsedData.area : '') + ' \u043C<sup>2</sup></span></div>\n\n                </div>\n\n                <div class="flats__flat-price">\n\n                  <p>\u0421\u0442\u043E\u0438\u043C\u043E\u0441\u0442\u044C:</p>\n\n                  <p>' + (splitDigits(parsedData.price) ? splitDigits(parsedData.price) : "0") + ' \u0440\u0443\u0431.*</p>\n\n                </div>\n\n    \n\n                <div class="flats__buttons-1">\n\n                  ' + $view + '\n\n                  <a href="' + $('.flats__popup').data('furnish-src') + '" class="flats__furniture"><div><p>\u0420\u0430\u0441\u0441\u0442\u0430\u043D\u043E\u0432\u043A\u0430<br/> \u043C\u0435\u0431\u0435\u043B\u0438</p></div></a>\n\n                </div>\n\n    \n\n                <div class="flats__buttons-2">\n\n                  <a href="" class="flats__button-apply">\u041E\u0441\u0442\u0430\u0432\u0438\u0442\u044C \u0437\u0430\u044F\u0432\u043A\u0443</a>\n\n                  <a href="' + parsedData.pdfPath + '" target="_blank" class="flats__button-download">\u0421\u043A\u0430\u0447\u0430\u0442\u044C \u043F\u043B\u0430\u043D\u0438\u0440\u043E\u0432\u043A\u0443</a>\n\n                </div>\n\n    \n\n                <ul>\n\n                  <li>* \u0426\u0435\u043D\u0430 \u0434\u0430\u043D\u0430 \u0441 \u0443\u0447\u0435\u0442\u043E\u043C \u0441\u043A\u0438\u0434\u043E\u043A</li>\n\n                  <li>* \u0420\u0435\u0437\u0435\u0440\u0432 \u043D\u0435 \u0444\u0438\u043A\u0441\u0438\u0440\u0443\u0435\u0442 \u0441\u0442\u043E\u0438\u043C\u043E\u0441\u0442\u044C \u0438 \u0443\u0441\u043B\u043E\u0432\u0438\u044F \u043E\u043F\u043B\u0430\u0442\u044B</li>\n\n                  <li>* \u0426\u0435\u043D\u0430 \u0434\u0435\u0439\u0441\u0442\u0432\u0438\u0442\u0435\u043B\u044C\u043D\u0430 \u043F\u0440\u0438 100% \u043E\u043F\u043B\u0430\u0442\u0435</li>\n\n                </ul>\n\n    \n\n    \n\n              </div>\n\n            ';

                popup.find('.flats__popup__container').html(content);

                if (checkDevice() !== 'desktop') {

                    $('body, html').css('overflow', 'hidden');

                    $('body,html').addClass('noscroll');
                }

                $('.flats__popup--close').unbind('click').on('click', function () {

                    if (checkDevice() !== 'desktop') {

                        $('body, html').removeAttr('style');

                        $('body,html').removeClass('noscroll');
                    }

                    popup.removeClass("flats__popup--visible");

                    history.back();
                });

                popup.addClass("flats__popup--visible");

                for (var item in parsedData) {

                    if (item === 'house' || item === 'section' || item === 'sectionName' || item === "num") {

                        filledInputsData[item] = parsedData[item];
                    }
                }

                inputForFilling.val('\u041A\u043E\u0440\u043F\u0443\u0441 \u2116 ' + filledInputsData.house + '; \u0421\u0435\u043A\u0446\u0438\u044F \u2116 ' + filledInputsData.section + '; \u041D\u0430\u0437\u0432\u0430\u043D\u0438\u0435 \u0441\u0435\u043A\u0446\u0438\u0438: ' + filledInputsData.sectionName + '; \u041A\u0432\u0430\u0440\u0442\u0438\u0440\u0430 \u2116 ' + filledInputsData.num);

                fancyboxDefaults();

                $('.flats__popup--close').on('click', function (e) {

                    if ($('body').hasClass('flat-mode')) {

                        if (document.referrer.match(/choose\/search/g) !== null) {

                            document.location = '/choose/search/';
                        } else {

                            e.preventDefault();

                            var url = window.location.href.split('/');

                            url.splice(-2);

                            url = url.join('/');

                            setTimeout(function () {

                                window.history.pushState('ChooseFlat', 'Выбор квартиры', url);
                            }, 100);
                        }
                    }
                });
            }).fail(function () {

                alert('Произошла какая-то ошибка. Попробуйте еще раз.');
            });
        });
    })();

    function splitDigits(num) {
        var str = num.toString().split('.');
        if (str[0].length >= 5) {
            str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1 ');
        }
        if (str[1] && str[1].length >= 5) {
            str[1] = str[1].replace(/(\d{3})/g, '$1 ');
        }
        return str.join('.');
    }

    (function () {

        $('#owl-carousel').owlCarousel({
            dots: false,
            nav: false,
            items: 1,
            navText: false,
            touchDrag: false,
            mouseDrag: false,
            autoplay: true,
            loop: true,
            autoplayTimeout: 4000,
            autoplaySpeed: 5000
        });
    })();

    // Automated popup revealing after coming from filter page
    (function () {

        var el = $('.flats__container__grid__wrapper__icon.current'),
            interval = setInterval(function () {
            if (el) {
                el.click();
                clearInterval(interval);
            } else {
                clearInterval(interval);
            }
        });
    })();

    (function () {

        var elem = $('.grid-total');

        elem.html('\u0421\u0432\u043E\u0431\u043E\u0434\u043D\u043E ' + $('.flats__container__grid__wrapper__icon').length + ' \u043A\u0432\u0430\u0440\u0442\u0438\u0440\u044B');
    })();

    // Svg korpus tooltips
    (function () {
        var text = void 0;

        $(document).tooltip({
            track: true,
            items: 'polygon',
            classes: {
                "ui-tooltip": "korpus-tooltip"
            },
            show: {
                duration: 100
            },
            hide: {
                duration: 100
            },
            open: function open(event, ui) {
                text = $(ui.tooltip).text();
                $(ui.tooltip).empty().append('<p>' + text + '</p>');
            }
        });
    })();

    // SVG POLYGONS HOVER (House plan)

    (function () {
        var oldColor = void 0;

        $('#korpus-plan polygon').on('mouseover', function () {
            oldColor = $(this).css('fill');
            $(this).css('fill', $(this).data('hover-color'));
        });
        $('#korpus-plan polygon').on('mouseout', function () {

            $(this).css('fill', oldColor);
        });
    })();

    // Lightbox—————————————————————————

    function fancyboxDefaults() {

        $('.flats__view[data-fancybox]').fancybox({
            touch: false,
            idleTime: 0,
            margin: [40, 40],
            buttons: ['close'],
            afterLoad: function afterLoad() {
                var imgWrapper = $('.fancybox-container .fancybox-image-wrap'),
                    el = $('.fancybox-container .fancybox-caption-wrap');

                imgWrapper.prepend(el);
                $('.fancybox-container').addClass('fancybox-container-flatview');
            },
            afterShow: function afterShow() {
                var imgWrapper = $('.fancybox-container .fancybox-image-wrap'),
                    el = $('.fancybox-container .fancybox-navigation');

                el.addClass('fancybox-navigation-visible');
                $('.fancybox-container .fancybox-caption-wrap').addClass('fancybox-caption-wrap--show');
                $('.fancybox-container .fancybox-navigation .count-wrapper').remove();
                $('.fancybox-container .fancybox-navigation').prepend('<div class="count-wrapper"><span class="image-index">' + (this.index + 1) + '</span>/<span class="image-total"> / ' + $('.fancybox-image').length + '</span></div>');
            },
            beforeClose: function beforeClose() {
                $('.fancybox-navigation-visible').removeClass('fancybox-navigation-visible');
            }
        });
    }

    (function () {
        $('.flats__container__content__more .fancybox-trigger').fancybox({

            idleTime: 0,
            margin: [40, 40],
            buttons: ['close'],
            clickContent: false,
            afterLoad: function afterLoad() {
                $('.fancybox-container').addClass('fancybox-container-furnish');
            }
        });
    })();

    (function () {

        $(document).click(function (e) {

            if ($(e.target).hasClass('flats__button-apply')) {
                $('#form-flat').modal({
                    close: true,
                    overlayClose: true
                });
                return false;
            }
        });
    })();
    //———————————————————————————————


    // Flat popup
    (function () {

        var popup = $('.flats__popup');

        $('.flats__popup--close').on('click', function () {
            if (checkDevice() !== 'desktop') {
                $('body, html').removeAttr('style');
            }
            popup.removeClass("flats__popup--visible");
            $('body,html').removeClass('noscroll');
        });
    })();
});