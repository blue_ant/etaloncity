"use strict";

$(document).ready(function () {

  var checkDevice = function checkDevice(options) {

    var result = void 0,
        ww = $(window).width(),
        current = {

      desktop: 1239,

      mobile: 767

    };

    if (options !== 'undefined') {

      for (var item in options) {

        current[item] = item;
      }
    }

    if (ww > current.desktop) {

      result = "desktop";
    } else if (ww < current.desktop && ww > current.mobile) {

      result = "tablet";
    } else if (ww < current.mobile) {

      result = "mobile";
    }

    return result;
  };

  (function () {
    $('.content #owl-carousel').owlCarousel({
      dots: true,
      nav: true,
      items: 1,
      navText: false,
      animateOut: 'fadeOut',
      touchDrag: true,
      mouseDrag: false,
      autoplay: true,
      loop: true,
      autoplayTimeout: 10000,
      autoplaySpeed: 5000
    });

    // $('#owl-carousel2').owlCarousel({
    //   dots: false,
    //   nav: true,
    //   items: 1,
    //   navText: false,
    //   touchDrag: true,
    //   mouseDrag: true,
    //   loop: true
    //   // autoplay: true,
    //   // autoplayTimeout: 4000,
    //   // autoplaySpeed: 5000
    // });

    $('.content #owl-carousel3').owlCarousel({
      dots: false,
      nav: true,
      items: 1,
      navText: false,
      touchDrag: true,
      mouseDrag: true,
      loop: true
      // autoplay: true,
      // autoplayTimeout: 4000,
      // autoplaySpeed: 5000
    });
  })();

  (function () {
    var el = $('.content');

    if ($('body').hasClass('page--fullscreen')) {

      if (checkDevice() === 'mobile') {
        $('body, html').on('swipeup', function () {
          el.css({
            'transform': 'translateY(-100vh)'
          });
        });

        $('body, html').on('swipedown', function () {
          el.css({
            'transform': 'translateY(0vh)'
          });
        });
      }
    }
  })();
});

// GOOGLE MAPS
var mapIsInited = false;
function gmapsInit() {

  var map = new GMaps({

    div: '.map',
    zoom: 14,
    lat: 55.558322,
    lng: 37.556446,
    styles: [{ "stylers": [{ "saturation": 50 }, { "lightness": 50 }, { "visibility": "on" }, { "weight": 2 }] }, { "elementType": "geometry", "stylers": [{ "color": "#ebe3cd" }, { "saturation": -45 }] }, { "elementType": "labels.icon", "stylers": [{ "saturation": 5 }, { "weight": 3.5 }] }, { "elementType": "labels.text.fill", "stylers": [{ "color": "#523735" }] }, { "elementType": "labels.text.stroke", "stylers": [{ "color": "#e7e2d9" }] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{ "color": "#c9b2a6" }] }, { "featureType": "administrative.land_parcel", "elementType": "geometry.stroke", "stylers": [{ "color": "#dcd2be" }] }, { "featureType": "administrative.land_parcel", "elementType": "labels.text.fill", "stylers": [{ "color": "#ae9e90" }] }, { "featureType": "landscape", "elementType": "labels.text", "stylers": [{ "saturation": -20 }, { "weight": 2 }] }, { "featureType": "landscape", "elementType": "labels.text.fill", "stylers": [{ "weight": 3 }] }, { "featureType": "landscape.natural", "elementType": "geometry", "stylers": [{ "color": "#dfd2ae" }] }, { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#dfd2ae" }] }, { "featureType": "poi", "elementType": "labels.text.fill", "stylers": [{ "color": "#93817c" }] }, { "featureType": "poi.park", "elementType": "geometry.fill", "stylers": [{ "color": "#a5b076" }] }, { "featureType": "poi.park", "elementType": "labels.text.fill", "stylers": [{ "color": "#1f3516" }] }, { "featureType": "road", "stylers": [{ "saturation": 50 }, { "lightness": 70 }, { "weight": 1 }] }, { "featureType": "road", "elementType": "geometry", "stylers": [{ "color": "#f5f1e6" }] }, { "featureType": "road.arterial", "stylers": [{ "weight": 2.5 }] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "color": "#fdfcf8" }] }, { "featureType": "road.highway", "elementType": "geometry", "stylers": [{ "color": "#f89d67" }] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "color": "#eec064" }] }, { "featureType": "road.highway.controlled_access", "elementType": "geometry", "stylers": [{ "color": "#e98d58" }] }, { "featureType": "road.highway.controlled_access", "elementType": "geometry.stroke", "stylers": [{ "color": "#d38152" }] }, { "featureType": "road.local", "stylers": [{ "color": "#e6ce8b" }] }, { "featureType": "road.local", "elementType": "labels.icon", "stylers": [{ "visibility": "simplified" }] }, { "featureType": "road.local", "elementType": "labels.text", "stylers": [{ "color": "#c6ac86" }] }, { "featureType": "road.local", "elementType": "labels.text.fill", "stylers": [{ "color": "#806b63" }] }, { "featureType": "transit.line", "elementType": "geometry", "stylers": [{ "color": "#dfd2ae" }] }, { "featureType": "transit.line", "elementType": "labels.text.fill", "stylers": [{ "color": "#8f7d77" }] }, { "featureType": "transit.line", "elementType": "labels.text.stroke", "stylers": [{ "color": "#ebe3cd" }] }, { "featureType": "transit.station", "elementType": "geometry", "stylers": [{ "color": "#dfd2ae" }] }, { "featureType": "water", "elementType": "geometry.fill", "stylers": [{ "color": "#b9d3c2" }] }, { "featureType": "water", "elementType": "labels.text.fill", "stylers": [{ "color": "#92998d" }] }]
  });

  //Car icon
  map.addMarker({
    lat: 55.557824,
    lng: 37.553446,
    icon: '../img/home/icon-car.png'

  });

  var overlay;
  USGSOverlay.prototype = new google.maps.OverlayView();

  function constructBounds(lat, lng) {
    var sw = new google.maps.LatLng(lat - .03, lng - .025);
    var ne = new google.maps.LatLng(lat + .03, lng + .025);
    return new google.maps.LatLngBounds(sw, ne);
  }

  var bounds = constructBounds(55.555824, 37.55700),
      img = '../img/home/icon-etalon.png',
      gmap = map;

  function USGSOverlay(bounds, image, map) {

    // Initialize all properties.
    this.bounds_ = bounds;
    this.image_ = image;
    this.map_ = map;

    // Define a property to hold the image's div. We'll
    // actually create this div upon receipt of the onAdd()
    // method so we'll leave it null for now.
    this.div_ = null;

    // Explicitly call setMap on this overlay.
  }

  USGSOverlay.prototype.onAdd = function () {

    var div = document.createElement('div');
    div.style.borderStyle = 'none';
    div.style.borderWidth = '0px';
    div.style.position = 'absolute';

    // Create the img element and attach it to the div.
    var img = document.createElement('img');
    img.src = this.image_;
    img.style.width = 'auto';
    img.style.height = '6%';
    img.style.position = 'absolute';
    img.style.top = '43%';
    img.style.left = '45%';
    div.appendChild(img);

    this.div_ = div;

    // Add the element to the "overlayLayer" pane.
    var panes = this.getPanes();
    panes.overlayLayer.appendChild(div);
  };

  USGSOverlay.prototype.draw = function () {

    // We use the south-west and north-east
    // coordinates of the overlay to peg it to the correct position and size.
    // To do this, we need to retrieve the projection from the overlay.
    var overlayProjection = this.getProjection();

    // Retrieve the south-west and north-east coordinates of this overlay
    // in LatLngs and convert them to pixel coordinates.
    // We'll use these coordinates to resize the div.
    var sw = overlayProjection.fromLatLngToDivPixel(this.bounds_.getSouthWest());
    var ne = overlayProjection.fromLatLngToDivPixel(this.bounds_.getNorthEast());

    // Resize the image's div to fit the indicated dimensions.
    var div = this.div_;
    div.style.left = sw.x + 'px';
    div.style.top = ne.y + 'px';
    div.style.width = ne.x - sw.x + 'px';
    div.style.height = sw.y - ne.y + 'px';
  };

  // var bounds = constructBounds(55.555824, 37.55700),
  //       img = '../img/home/icon-etalon.png',
  //       gmap = map;

  var bro = new USGSOverlay(bounds, img, gmap).setMap(map.map);

  // var staticOverlay = new google.maps.GroundOverlay('../img/home/icon-etalon.png', constructBounds(55.555824, 37.55700));
  // staticOverlay.setMap(map.map);


  // var overlay;
  // USGSOverlay.prototype = new google.maps.OverlayView();

  // overlay = new USGSOverlay(constructBounds(55.555824, 37.55700), '../img/home/icon-etalon.png', map);


  // ————————————————————————————————————————————————

  //Tree icon
  map.addMarker({
    lat: 55.557267,
    lng: 37.564101,
    icon: '../img/home/icon-tree.png'
  });

  //Gas icon
  map.addMarker({
    lat: 55.553,
    lng: 37.571363,
    icon: '../img/home/icon-gas.png'
  });

  //Kindergarden icon
  map.addMarker({
    lat: 55.547214,
    lng: 37.564545,
    icon: '../img/home/icon-kindergarden.png'
  });

  //Shop icon
  map.addMarker({
    lat: 55.56405,
    lng: 37.555,
    icon: '../img/home/icon-shop.png'
  });

  //Subway icons
  //Bottom
  map.addMarker({
    lat: 55.54740,
    lng: 37.555066,
    icon: '../img/home/icon-subway.png'
  });
  //Top
  map.addMarker({
    lat: 55.569029,
    lng: 37.578260,
    icon: '../img/home/icon-subway.png'
  });

  //Выезд на МКАД
  var tooltipOne = map.drawOverlay({
    lat: 55.580795,
    lng: 37.558090,
    content: '<div class="tooltip-1">ОТДЕЛЬНЫЙ ВЫЕЗД НА МКАД<br/>7 минут на авто</div>'
  });

  //Бульвар Дмитрия Донского
  var tooltipTwo = map.drawOverlay({
    lat: 55.566663,
    lng: 37.586593,
    content: '<div class="tooltip-2">М. БУЛЬВАР ДМИТРИЯ ДОНСКОГО<br/>10 минут на автобусе</div>'
  });

  var tooltipThree = map.drawOverlay({
    lat: 55.54800,
    lng: 37.545766,
    content: '<div class="tooltip-3">М. СКОБЕЛЕВСКАЯ<br/>15 минут пешком</div>'
  });

  map.drawPolyline({
    path: [[55.580752, 37.573325], [55.580561, 37.572772], [55.580422, 37.572536], [55.578448, 37.569800], [55.577775, 37.568765], [55.577353, 37.567971], [55.576189, 37.566448], [55.575021, 37.565209], [55.573805, 37.564249], [55.565241, 37.558210], [55.564881, 37.558080], [55.564110, 37.557707]],
    strokeColor: '#009B42',
    strokeOpacity: 1,
    strokeWeight: 5
  });

  map.drawPolyline({
    path: [[55.569502, 37.578485], [55.569854, 37.576704], [55.566480, 37.574190], [55.569196, 37.562923], [55.569152, 37.562497], [55.566611, 37.560602], [55.566253, 37.560193], [55.565361, 37.558739], [55.564606, 37.558061], [55.561760, 37.555970], [55.560916, 37.555455], [55.558583, 37.554516]],
    strokeColor: '#EF1919',
    strokeOpacity: 1,
    strokeWeight: 5
  });

  map.drawPolyline({
    path: [[55.54790, 37.555266], [55.548999, 37.554929], [55.550789, 37.554787], [55.551505, 37.554487], [55.552258, 37.553371], [55.552640, 37.552985], [55.553550, 37.552438], [55.553880, 37.552508], [55.557448, 37.554028], [55.557352, 37.554401]],
    strokeColor: '#EF1919',
    strokeOpacity: 1,
    strokeWeight: 5
  });

  map.addListener('zoom_changed', function () {
    var zoom = map.getZoom();
  });

  mapIsInited = true;
}

(function () {
  $.fancybox.defaults.touch = false;
  $.fancybox.defaults.smallBtn = false;
  $.fancybox.defaults.animationEffect = 'fade';
  $.fancybox.defaults.animationDuration = 400;

  if (!mapIsInited) {
    $('.trigger-map').fancybox({
      afterLoad: gmapsInit
    });
  }
})();