"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

$(document).ready(function () {

    var checkDevice = function checkDevice(options) {

        var result = void 0,
            ww = $(window).width(),
            current = {

            desktop: 1239,

            mobile: 767

        };

        if (options !== 'undefined') {

            for (var item in options) {

                current[item] = item;
            }
        }

        if (ww > current.desktop) {

            result = "desktop";
        } else if (ww < current.desktop && ww > current.mobile) {

            result = "tablet";
        } else if (ww < current.mobile) {

            result = "mobile";
        }

        return result;
    };

    function splitDigits(num) {
        var str = num.toString().split('.');
        if (str[0].length >= 5) {
            str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1 ');
        }
        if (str[1] && str[1].length >= 5) {
            str[1] = str[1].replace(/(\d{3})/g, '$1 ');
        }
        return str.join('.');
    }

    function completeFillTable(house, tableIndex) {

        var $table = $(".table[data-table=\"" + tableIndex + "\"]");

        try {
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {

                for (var _iterator = house.flats[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var flat = _step.value;


                    $(".table[data-table=\"" + tableIndex + "\"]").DataTable().row.add(["" + (flat.rooms !== 0 ? flat.rooms : '<span hidden>0</span> Студия'), house.houseName, flat.floor, flat.area, flat.price]).draw().node();

                    if (flat.discount) {

                        // $('tbody tr:last-of-type', $table).addClass('has-action');

                    }
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }
        } catch (err) {

            console.error(err);
        }
    }

    function createTable(house) {
        var index = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'none';


        var $tableMarkup = void 0,
            $button = void 0;

        $tableMarkup = "\n\n    \n\n          <p class=\"sidebar__result__info-1\">" + house.houseName + "</p>\n\n          <p class=\"sidebar__result__info-2 " + (house.finish ? '' : 'hidden') + "\">\u0414\u043E\u043C \u0441\u0434\u0430\u043D</p>\n\n          <p>\u0421\u0432\u043E\u0431\u043E\u0434\u043D\u043E " + house.total + " \u043A\u0432.</p>\n\n    \n\n          <div class=\"table-wrapper\">\n\n    \n\n            <table data-table=\"" + index + "\" class=\"table\">\n\n              <thead>\n\n                <tr>\n\n                  <th>\u041A\u043E\u043C\u043D\u0430\u0442</th>\n\n                  <th>\u0421\u0435\u043A\u0446\u0438\u044F</th>\n\n                  <th>\u042D\u0442\u0430\u0436</th>\n\n                  <th>\u041F\u043B\u043E\u0449\u0430\u0434\u044C</th>\n\n                  <th>\u0426\u0435\u043D\u0430*</th>\n\n                </tr>\n\n              </thead>\n\n              <tbody></tbody>\n\n            </table>\n\n        ";

        if (house.flats.length > 4) {

            $button = "\n\n              <a href=\"\" data-table-button=\"" + index + "\" data-step=\"1\" class=\"show-more\">\u041F\u043E\u043A\u0430\u0437\u0430\u0442\u044C \u0435\u0449\u0451</a>\n\n    \n\n            </div>\n\n          ";
        } else {

            $button = "</div>";
        }

        $tableMarkup += $button;

        return $tableMarkup;
    }

    function initFillTable(house, table, tableIndex) {

        var $table = $(table);

        if (!house) {

            throw new Error('House doesn\'t exist');
        }

        try {
            var _iteratorNormalCompletion2 = true;
            var _didIteratorError2 = false;
            var _iteratorError2 = undefined;

            try {

                for (var _iterator2 = house.flats[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                    var flat = _step2.value;


                    $table.DataTable().row.add(["" + (flat.rooms !== 0 ? flat.rooms : '<span hidden>0</span> Студия'), house.houseName, flat.floor, flat.area, flat.price]).draw().node();

                    // if (flat.discount) {

                    //  $('tbody tr:last-of-type', $table).addClass('has-action');

                    // }
                }
            } catch (err) {
                _didIteratorError2 = true;
                _iteratorError2 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion2 && _iterator2.return) {
                        _iterator2.return();
                    }
                } finally {
                    if (_didIteratorError2) {
                        throw _iteratorError2;
                    }
                }
            }
        } catch (err) {

            console.error(err);
        }
    }

    function insertTable(table, wrapper) {

        var $table = $(table),
            $wrapper = $(wrapper);

        if (!$table) {

            throw new Error("You haven't specified table or table doesn't exist");
        } else if (!$wrapper) {

            throw new Error("You haven't specified wrapper or wrapper doesn't exist");
        }

        $($table).appendTo($wrapper);
    }

    function splitDigits(num) {

        var str = num.toString().split('.');

        if (str[0].length >= 5) {

            str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1 ');
        }

        if (str[1] && str[1].length >= 5) {

            str[1] = str[1].replace(/(\d{3})/g, '$1 ');
        }

        return str.join('.');
    }

    function makeTableSortable(tableMarkup) {

        var $table = $(tableMarkup);

        if (!$table.hasClass('sorting-inited')) {

            try {
                var _$table$DataTable;

                $table.DataTable((_$table$DataTable = {

                    'searching': false,

                    'bPaginate': false,

                    'bLengthChange': false,

                    'bInfo': false,

                    "paging": false

                }, _defineProperty(_$table$DataTable, "searching", false), _defineProperty(_$table$DataTable, 'createdRow', function createdRow(row, data, dataIndex) {

                    $(row).find('td').eq(3).html(data[3] + ' м<sup>2</sup>');

                    $(row).find('td').eq(4).html(splitDigits(parseInt(data[4], 10)));
                }), _$table$DataTable));

                $table.addClass('sorting-inited');
            } catch (err) {

                console.error(err);
            }
        } else {

            throw new Error('This table is already sortable');
        }
    }

    (function () {

        var links = $('[data-mobile-src]');

        links.on('click', function (e) {

            if (checkDevice() !== 'desktop') {

                e.preventDefault();

                var urlTo = $(this).data('mobile-src');

                document.location.href = urlTo;
            } else {
                return;
            }
        });
    })();

    // Initializing custom scrollbar for tables in sidebar

    function customSidebarScroll() {

        var elem = $('.sidebar .sidebar__wrapper');

        if (elem) {
            if ($(window).width() < 1023) {
                var _elem$niceScroll;

                elem.getNiceScroll().remove();

                elem.niceScroll((_elem$niceScroll = {
                    cursorcolor: "gray",
                    cursorfixedheight: 100,
                    cursoropacitymin: 0.2
                }, _defineProperty(_elem$niceScroll, "cursoropacitymin", 0.2), _defineProperty(_elem$niceScroll, "cursorwidth", 4), _defineProperty(_elem$niceScroll, "preventmultitouchscrolling", false), _defineProperty(_elem$niceScroll, "horizrailenabled", true), _defineProperty(_elem$niceScroll, "cursorborder", '0px solid transparent'), _defineProperty(_elem$niceScroll, "railoffset", {
                    left: 15,
                    top: 0,
                    right: 0,
                    bottom: 0
                }), _elem$niceScroll));
            } else {
                var _elem$niceScroll2;

                elem.getNiceScroll().remove();

                elem.niceScroll((_elem$niceScroll2 = {
                    cursorcolor: "gray",
                    cursorfixedheight: 100,
                    cursoropacitymin: 0.2
                }, _defineProperty(_elem$niceScroll2, "cursoropacitymin", 0.2), _defineProperty(_elem$niceScroll2, "cursorwidth", 4), _defineProperty(_elem$niceScroll2, "preventmultitouchscrolling", false), _defineProperty(_elem$niceScroll2, "horizrailenabled", true), _defineProperty(_elem$niceScroll2, "cursorborder", '0px solid transparent'), _defineProperty(_elem$niceScroll2, "railoffset", {
                    left: 15,
                    top: 0,
                    right: 0,
                    bottom: 0
                }), _elem$niceScroll2));
            }
        }
    }

    // Adding value range for sliders in sidebar
    (function () {
        var sliderWrappers = [$('.sidebar__slider-1'), $('.sidebar__slider-2'), $('.sidebar__slider-3')],
            sliderRangeNumWrappers = [$('.sidebar__slider-1__values'), $('.sidebar__slider-2__values'), $('.sidebar__slider-3__values')];

        var sliderStartValues = [{
            min: sliderWrappers[0].data('min'),
            max: sliderWrappers[0].data('max')
        }, {
            min: sliderWrappers[1].data('min'),
            max: sliderWrappers[1].data('max')
        }, {
            min: sliderWrappers[2].data('min'),
            max: sliderWrappers[2].data('max')
        }];

        var rangeValues = [[sliderStartValues[0].min, parseInt(((sliderStartValues[0].min + sliderStartValues[0].max) / 2 + sliderStartValues[0].min) / 2, 10), parseInt((sliderStartValues[0].min + sliderStartValues[0].max) / 2, 10), parseInt(((sliderStartValues[0].min + sliderStartValues[0].max) / 2 + sliderStartValues[0].max) / 2, 10), sliderStartValues[0].max], [sliderStartValues[1].min, parseInt(((sliderStartValues[1].min + sliderStartValues[1].max) / 2 + sliderStartValues[1].min) / 2, 10), parseInt((sliderStartValues[1].min + sliderStartValues[1].max) / 2, 10), parseInt(((sliderStartValues[1].min + sliderStartValues[1].max) / 2 + sliderStartValues[1].max) / 2, 10), sliderStartValues[1].max], [sliderStartValues[2].min, parseInt(((sliderStartValues[2].min + sliderStartValues[2].max) / 2 + sliderStartValues[2].min) / 2, 10), parseInt((sliderStartValues[2].min + sliderStartValues[2].max) / 2, 10), parseInt(((sliderStartValues[2].min + sliderStartValues[2].max) / 2 + sliderStartValues[2].max) / 2, 10), sliderStartValues[2].max]];

        if (sliderWrappers[0] && rangeValues[0][0]) {
            try {
                sliderRangeNumWrappers.forEach(function (el, index) {

                    for (var i = 0; i < 5; i++) {
                        var small = document.createElement('small');
                        small.innerHTML = splitDigits(rangeValues[index][i] + '');
                        el.append(small);
                    }
                });
            } catch (err) {
                console.error(err);
            }
        }
    })();

    // Toggle sidebar trigger

    (function () {
        var button = $('.sidebar-trigger');
        var sidebar = $('aside.sidebar');

        if (typeof button !== 'undefined' && typeof sidebar !== 'undefined') {
            button.on('click', function () {

                button.toggleClass('sidebar-trigger--active');
                sidebar.toggleClass('sidebar--active');
            });
        }
    })();

    // Redirect to filled towers

    (function () {
        var towers = $('.pin_tower').closest('a');
        var hrefs = [];

        var _iteratorNormalCompletion3 = true;
        var _didIteratorError3 = false;
        var _iteratorError3 = undefined;

        try {
            for (var _iterator3 = towers[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                var item = _step3.value;

                hrefs.push($(item).attr('href'));
            }
        } catch (err) {
            _didIteratorError3 = true;
            _iteratorError3 = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion3 && _iterator3.return) {
                    _iterator3.return();
                }
            } finally {
                if (_didIteratorError3) {
                    throw _iteratorError3;
                }
            }
        }

        hrefs.sort();

        towers.on('click', function (e) {
            if ($(this).hasClass('has-no-flats')) {
                e.preventDefault();
                var self = $(this),
                    num = parseInt($(this).text().trim().slice(-1), 10),
                    leadToLink = void 0;

                switch (num) {
                    case 1:
                        leadToLink = hrefs[1];
                        break;
                    case 2:
                        leadToLink = hrefs[2];
                        break;
                    case 3:
                        leadToLink = hrefs[3];
                        break;
                    case 4:
                        leadToLink = hrefs[4];
                        break;
                    case 5:
                        leadToLink = hrefs[5];
                        break;
                    case 6:
                        leadToLink = hrefs[0];
                        break;
                }

                document.location.pathname = leadToLink;
            } else {
                return;
            }
        });
    })();

    // Initialization of modals with panorama

    (function () {

        var modals = [$("#modal-1-init"), $("#modal-2-init"), $("#modal-3-init"), $("#modal-4-init"), $("#modal-5-init"), $("#modal-6-init"), $("#modal-7-init")];

        if (modals[0]) {

            modals[0].animatedModal({
                modalTarget: 'modal-1',
                animatedIn: 'bounceInUp',
                animatedOut: 'zoomOut',
                color: 'white'
            });
        }

        if (modals[1]) {

            modals[1].animatedModal({
                modalTarget: 'modal-2',
                animatedIn: 'bounceInUp',
                animatedOut: 'zoomOut',
                color: 'white',
                beforeOpen: function beforeOpen() {
                    if ($('#panorama-2').is(':empty')) {
                        pannellum.viewer('panorama-2', {
                            "type": "equirectangular",
                            "panorama": "/img/choose/cam-2.jpg",
                            'autoLoad': true,
                            'showControls': false
                        });
                    }
                }
            });
        }

        if (modals[2]) {

            modals[2].animatedModal({
                modalTarget: 'modal-3',
                animatedIn: 'bounceInUp',
                animatedOut: 'zoomOut',
                color: 'white',
                beforeOpen: function beforeOpen() {
                    if ($('#panorama-3').is(':empty')) {
                        pannellum.viewer('panorama-3', {
                            "type": "equirectangular",
                            "panorama": "/img/choose/cam-3.jpg",
                            'autoLoad': true,
                            'showControls': false
                        });
                    }
                }
            });
        }

        if (modals[3]) {

            modals[3].animatedModal({
                modalTarget: 'modal-4',
                animatedIn: 'bounceInUp',
                animatedOut: 'zoomOut',
                color: 'white',
                beforeOpen: function beforeOpen() {
                    if ($('#panorama-4').is(':empty')) {
                        pannellum.viewer('panorama-4', {
                            "type": "equirectangular",
                            "panorama": "/img/choose/cam-4.jpg",
                            'autoLoad': true,
                            'showControls': false
                        });
                    }
                }
            });
        }

        if (modals[4]) {

            modals[4].animatedModal({
                modalTarget: 'modal-5',
                animatedIn: 'bounceInUp',
                animatedOut: 'zoomOut',
                color: 'white',
                beforeOpen: function beforeOpen() {
                    if ($('#panorama-5').is(':empty')) {
                        pannellum.viewer('panorama-5', {
                            "type": "equirectangular",
                            "panorama": "/img/choose/cam-5.jpg",
                            'autoLoad': true,
                            'showControls': false
                        });
                    }
                }
            });
        }

        if (modals[5]) {

            modals[5].animatedModal({
                modalTarget: 'modal-6',
                animatedIn: 'bounceInUp',
                animatedOut: 'zoomOut',
                color: 'white',
                beforeOpen: function beforeOpen() {
                    if ($('#panorama-6').is(':empty')) {
                        pannellum.viewer('panorama-6', {
                            "type": "equirectangular",
                            "panorama": "/img/choose/cam-6.jpg",
                            'autoLoad': true,
                            'showControls': false
                        });
                    }
                }
            });
        }

        if (modals[6]) {

            modals[6].animatedModal({
                modalTarget: 'modal-7',
                animatedIn: 'zoomIn',
                animatedOut: 'zoomOut',
                color: 'white'
            });
        }
    })();

    // Block-pusher of markers during resize


    (function () {

        var fitblockEl = $('.choose-content__fitblock').get(0);

        if (fitblockEl) {
            try {
                var Fitblock = function () {
                    function Fitblock(el) {
                        _classCallCheck(this, Fitblock);

                        this.outer = el;
                        this.inner = el.getElementsByClassName('choose-content__fitblock__inner')[0];
                        this.pusher = this.inner.getElementsByClassName('fitblock__pusher')[0];
                        this.refresh();
                    }

                    _createClass(Fitblock, [{
                        key: "refresh",
                        value: function refresh() {
                            var _this = this;

                            window.requestAnimationFrame(function () {
                                var outer = _this.outer;
                                var inner = _this.inner;
                                var pusher = _this.pusher;
                                var wrapH = outer.offsetHeight;
                                var wrapW = outer.offsetWidth;

                                var pusherH = pusher.height;
                                var pusherW = pusher.width;
                                var rel = pusherW / pusherH;
                                if (wrapW / pusherW > wrapH / pusherH) {
                                    pusher.style.width = wrapW + "px";
                                    pusher.style.height = "auto";
                                    inner.style.marginLeft = "-" + wrapW / 2 + "px";
                                    inner.style.marginTop = "-" + wrapW / rel / 2 + "px";
                                } else {
                                    pusher.style.width = "auto";
                                    pusher.style.height = wrapH + "px";
                                    inner.style.marginLeft = "-" + wrapH * rel / 2 + "px";
                                    inner.style.marginTop = "-" + wrapH / 2 + "px";
                                }

                                inner.style.visibility = "visible";
                            });
                        }
                    }]);

                    return Fitblock;
                }();

                var mapFitInstance = new Fitblock(fitblockEl);

                $(window).on('resize orientationchange', function () {
                    mapFitInstance.refresh();
                });
                $(window).on('orientationchange', function () {
                    $('.choose-content__fitblock').scrollLeft(0);
                });
                $(window).trigger('resize');
            } catch (err) {
                console.error(err);
            }
        }
    })();

    // AJAX update of tables

    function moreFlats(houseNum, offset) {

        function getRooms() {
            var arr = [];

            $('aside.sidebar #check-1').is(':checked') ? arr.push(0) : false;
            $('aside.sidebar #check-2').is(':checked') ? arr.push(1) : false;
            $('aside.sidebar #check-3').is(':checked') ? arr.push(2) : false;
            $('aside.sidebar #check-4').is(':checked') ? arr.push(3) : false;

            return arr;
        }

        var house = void 0,
            offsetReady = offset,
            loader = $('.loader-genplan'),
            $tablesWrapper = $('.filter .container'),
            opts = {
            get_flats: 'json',
            limit: 10,
            offset: offsetReady,
            house: houseNum,
            priceFrom: $('aside.sidebar [name="price-from"]').val(),
            priceTo: $('aside.sidebar [name="price-to"]').val(),
            priceMeterFrom: $('aside.sidebar [name="price-meter-from"]').val(),
            priceMeterTo: $('aside.sidebar [name="price-meter-to"]').val(),
            floorFrom: $('aside.sidebar [name="floor-from"]').val(),
            floorTo: $('aside.sidebar [name="floor-to"]').val(),
            rooms: getRooms(), //массив
            sqFrom: $('aside.sidebar [name="sq-from"]').val(),
            sqTo: $('aside.sidebar [name="sq-to"]').val(),
            furnish: "" + ($('aside.sidebar #checkbox-1').is(':checked') ? '1' : '0'),
            flatview: "" + ($('aside.sidebar #checkbox-2').is(':checked') ? '1' : '0'),
            discount: "" + ($('aside.sidebar #checkbox-3').is(':checked') ? '1' : '0')
        };

        $.ajax({
            method: "GET",
            url: '/choose/search/',
            data: opts,
            dataType: 'json',
            beforeSend: function beforeSend() {
                loader.addClass('loader--active');
            }
        }).done(function (response) {
            try {
                house = response.houses[0];
            } catch (err) {
                console.error(err);
            }

            if (house.flats.length === 0) {
                $(".show-more[data-table-button=\"" + (houseNum - 1) + "\"]").remove();
                loader.removeClass('loader--active');
                return;
            }

            completeFillTable(house, houseNum - 1);
            customSidebarScroll();
            loader.removeClass('loader--active');

            if (house.flats.length < 10) {
                $(".show-more[data-table-button=\"" + (houseNum - 1) + "\"]").remove();
            }
        });
    }

    function getFlats() {

        var houses = [],
            markup = [],
            totalFound = 0,
            $totalFound = $('.sidebar__result .sidebar__result__found'),
            $tablesWrapper = $('.sidebar__result__wrapper'),
            loader = $('.loader-genplan');

        function getRooms() {
            var arr = [];

            $('aside.sidebar #check-1').is(':checked') ? arr.push(0) : false;
            $('aside.sidebar #check-2').is(':checked') ? arr.push(1) : false;
            $('aside.sidebar #check-3').is(':checked') ? arr.push(2) : false;
            $('aside.sidebar #check-4').is(':checked') ? arr.push(3) : false;

            return arr;
        }

        var sidebar = $("aside.sidebar"),
            opts = {
            get_flats: 'json',
            priceFrom: $('aside.sidebar [name="price-from"]').val(),
            priceTo: $('aside.sidebar [name="price-to"]').val(),
            priceMeterFrom: $('aside.sidebar [name="price-meter-from"]').val(),
            priceMeterTo: $('aside.sidebar [name="price-meter-to"]').val(),
            floorFrom: $('aside.sidebar [name="floor-from"]').val(),
            floorTo: $('aside.sidebar [name="floor-to"]').val(),
            rooms: getRooms(), //массив
            sqFrom: $('aside.sidebar [name="sq-from"]').val(),
            sqTo: $('aside.sidebar [name="sq-to"]').val(),
            furnish: "" + ($('aside.sidebar #checkbox-1').is(':checked') ? '1' : '0'),
            flatview: "" + ($('aside.sidebar #checkbox-2').is(':checked') ? '1' : '0'),
            discount: "" + ($('aside.sidebar #checkbox-3').is(':checked') ? '1' : '0')
        };

        $.ajax({
            method: "GET",
            url: '/choose/search/',
            data: opts,
            dataType: 'json',
            beforeSend: function beforeSend() {
                $($tablesWrapper).empty();
                $totalFound.html('Поиск...');
                loader.addClass('loader-genplan--active');
                customSidebarScroll();
                $('.sidebar__wrapper').getNiceScroll().resize();
            }
        }).done(function (response) {

            try {
                houses = response.houses;
                var _iteratorNormalCompletion4 = true;
                var _didIteratorError4 = false;
                var _iteratorError4 = undefined;

                try {
                    for (var _iterator4 = houses[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                        var house = _step4.value;

                        totalFound += house.total;
                    }
                } catch (err) {
                    _didIteratorError4 = true;
                    _iteratorError4 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion4 && _iterator4.return) {
                            _iterator4.return();
                        }
                    } finally {
                        if (_didIteratorError4) {
                            throw _iteratorError4;
                        }
                    }
                }
            } catch (err) {
                console.error(err);
            }

            $($tablesWrapper).empty();
            $totalFound.html("\u041D\u0430\u0439\u0434\u0435\u043D\u043E " + totalFound + " \u043A\u0432\u0430\u0440\u0442\u0438\u0440");

            houses.forEach(function (item, index) {
                if (item.flats.length === 0) return;

                var $table = createTable(item, index);

                insertTable($table, $tablesWrapper);
                makeTableSortable($(".table[data-table='" + index + "']"));
                initFillTable(item, $(".table[data-table='" + index + "']"), index);

                $(".show-more[data-table-button=\"" + index + "\"]").unbind('click').click(function (event) {
                    var offset = $(".table[data-table=\"" + index + "\"] tbody tr").get().length;
                    event.preventDefault();
                    moreFlats(index + 1, offset);
                    offset += 10;
                });
            });
            customSidebarScroll();
            loader.removeClass('loader-genplan--active');
        }).fail(function (err) {
            throw err;
        });
    }

    // Initialization of sliders in sidebar
    (function () {

        var one = $('#slider-1'),
            two = $('#slider-2'),
            three = $('#slider-3');

        if (one) {

            var val = [$('.sidebar__slider-1').data('min'), $('.sidebar__slider-1').data('max')];
            one.slider({
                min: val[0],
                max: val[1],
                range: true,
                values: [val[0], val[1]],
                slide: function slide(event, ui) {
                    $('.sidebar__slider-1__inputs input:first-of-type').val(ui.values[0]);
                    $('.sidebar__slider-1__inputs input:last-of-type').val(ui.values[1]);
                },
                stop: function stop(event, ui) {
                    getFlats();
                }
            });
        }

        if (two) {

            var _val = [$('.sidebar__slider-2').data('min'), $('.sidebar__slider-2').data('max')];
            two.slider({
                min: _val[0],
                max: _val[1],
                range: true,
                values: [_val[0], _val[1]],
                slide: function slide(event, ui) {
                    $('.sidebar__slider-2__inputs input:first-of-type').val(ui.values[0]);
                    $('.sidebar__slider-2__inputs input:last-of-type').val(ui.values[1]);
                },
                stop: function stop() {
                    getFlats();
                }
            });
        }

        if (three) {

            var _val2 = [$('.sidebar__slider-3').data('min'), $('.sidebar__slider-3').data('max')];
            three.slider({
                min: _val2[0],
                max: _val2[1],
                range: true,
                values: [_val2[0], _val2[1]],
                slide: function slide(event, ui) {
                    $('.sidebar__slider-3__inputs input:first-of-type').val(ui.values[0]);
                    $('.sidebar__slider-3__inputs input:last-of-type').val(ui.values[1]);
                },
                stop: function stop() {
                    getFlats();
                }
            });
        }

        $('aside.sidebar-choose input').on('change', function () {
            getFlats();
        });
    })();

    // Filter on cha
    (function () {
        getFlats();
    })();
});