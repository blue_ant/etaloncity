'use strict';

function createTable(house) {
    var index = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'none';

    var $tableMarkup = void 0,
        $button = void 0;

    $tableMarkup = '\n\n      <p class="sidebar__result__info-1">' + house.houseName + '</p>\n      <p class="sidebar__result__info-2 ' + (house.finish ? '' : 'hidden') + '">\u0414\u043E\u043C \u0441\u0434\u0430\u043D</p>\n      <p>\u0421\u0432\u043E\u0431\u043E\u0434\u043D\u043E ' + house.total + ' \u043A\u0432.</p>\n\n      <div class="table-wrapper">\n\n        <table data-table="' + index + '" class="table">\n          <thead>\n            <tr>\n              <th>\u041A\u043E\u043C\u043D\u0430\u0442</th>\n              <th>\u0421\u0435\u043A\u0446\u0438\u044F</th>\n              <th>\u042D\u0442\u0430\u0436</th>\n              <th>\u041F\u043B\u043E\u0449\u0430\u0434\u044C</th>\n              <th>\u0426\u0435\u043D\u0430*</th>\n            </tr>\n          </thead>\n          <tbody></tbody>\n        </table>\n    ';

    if (house.flats.length > 4) {
        $button = '\n          <a href="" data-table-button="' + index + '" data-step="1" class="show-more">\u041F\u043E\u043A\u0430\u0437\u0430\u0442\u044C \u0435\u0449\u0451</a>\n\n        </div>\n      ';
    } else {
        $button = '</div>';
    }

    $tableMarkup += $button;

    return $tableMarkup;
}