'use strict';

function completeFillTable(house, tableIndex) {

	var $table = $('.table[data-table="' + tableIndex + '"]');

	try {
		var _iteratorNormalCompletion = true;
		var _didIteratorError = false;
		var _iteratorError = undefined;

		try {

			for (var _iterator = house.flats[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
				var flat = _step.value;

				$('.table[data-table="' + tableIndex + '"]').DataTable().row.add(['' + (flat.rooms !== 0 ? flat.rooms : '<span hidden>0</span> Студия'), house.houseName, flat.floor, flat.area, flat.price]).draw().node();
				if (flat.discount) {
					// $('tbody tr:last-of-type', $table).addClass('has-action');
				}
			}
		} catch (err) {
			_didIteratorError = true;
			_iteratorError = err;
		} finally {
			try {
				if (!_iteratorNormalCompletion && _iterator.return) {
					_iterator.return();
				}
			} finally {
				if (_didIteratorError) {
					throw _iteratorError;
				}
			}
		}
	} catch (err) {
		console.error(err);
	}
}