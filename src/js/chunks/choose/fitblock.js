'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Fitblock = function () {
    function Fitblock(el) {
        _classCallCheck(this, Fitblock);

        this.outer = el;
        this.inner = el.getElementsByClassName('choose-content__fitblock__inner')[0];
        this.pusher = this.inner.getElementsByClassName('fitblock__pusher')[0];
        this.refresh();
    }

    _createClass(Fitblock, [{
        key: 'refresh',
        value: function refresh() {
            var _this = this;

            window.requestAnimationFrame(function () {
                var outer = _this.outer;
                var inner = _this.inner;
                var pusher = _this.pusher;
                var wrapH = outer.offsetHeight;
                var wrapW = outer.offsetWidth;

                var pusherH = pusher.height;
                var pusherW = pusher.width;
                var rel = pusherW / pusherH;
                if (wrapW / pusherW > wrapH / pusherH) {
                    pusher.style.width = wrapW + 'px';
                    pusher.style.height = "auto";
                    inner.style.marginLeft = '-' + wrapW / 2 + 'px';
                    inner.style.marginTop = '-' + wrapW / rel / 2 + 'px';
                } else {
                    pusher.style.width = "auto";
                    pusher.style.height = wrapH + 'px';
                    inner.style.marginLeft = '-' + wrapH * rel / 2 + 'px';
                    inner.style.marginTop = '-' + wrapH / 2 + 'px';
                }

                inner.style.visibility = "visible";
            });
        }
    }]);

    return Fitblock;
}();