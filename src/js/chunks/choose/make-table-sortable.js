'use strict';

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function splitDigits(num) {
    var str = num.toString().split('.');
    if (str[0].length >= 5) {
        str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1 ');
    }
    if (str[1] && str[1].length >= 5) {
        str[1] = str[1].replace(/(\d{3})/g, '$1 ');
    }
    return str.join('.');
}

function makeTableSortable(tableMarkup) {
    var $table = $(tableMarkup);

    if (!$table.hasClass('sorting-inited')) {

        try {
            var _$table$DataTable;

            $table.DataTable((_$table$DataTable = {
                'searching': false,
                'bPaginate': false,
                'bLengthChange': false,
                'bInfo': false,
                "paging": false
            }, _defineProperty(_$table$DataTable, 'searching', false), _defineProperty(_$table$DataTable, 'createdRow', function createdRow(row, data, dataIndex) {
                $(row).find('td').eq(3).html(data[3] + ' м<sup>2</sup>');
                $(row).find('td').eq(4).html(splitDigits(parseInt(data[4], 10)));
            }), _$table$DataTable));

            $table.addClass('sorting-inited');
        } catch (err) {
            console.error(err);
        }
    } else {
        throw new Error('This table is already sortable');
    }
}