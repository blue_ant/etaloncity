'use strict';

function completeFillTable(house, tableIndex, dimension) {
    var $tableDesktop = $('.table-tablet[data-table="' + tableIndex + '"]'),
        $tableMobile = $('.table-mobile[data-table="' + tableIndex + '"]');
    console.log(house);

    if (!house.flats instanceof Array) {
        throw new Error('Flats are not presented as an array');
    } else if (house.flats.length === 0) {
        throw new Error('Flats array is empty');
    }

    if (dimension !== 'mobile') {
        var _iteratorNormalCompletion = true;
        var _didIteratorError = false;
        var _iteratorError = undefined;

        try {

            for (var _iterator = house.flats[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                var flat = _step.value;

                $tableDesktop.DataTable().row.add(['' + (flat.rooms !== 0 ? flat.rooms + ' ком.' : '<span hidden>0</span> Студия'), flat.floor, flat.area + ' \u043C<sup>2</sup>', splitDigits(flat.price) + ' \u0440\u0443\u0431.', splitDigits(flat.pricem2) + ' \u0440\u0443\u0431.']).draw().node();
            }
        } catch (err) {
            _didIteratorError = true;
            _iteratorError = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion && _iterator.return) {
                    _iterator.return();
                }
            } finally {
                if (_didIteratorError) {
                    throw _iteratorError;
                }
            }
        }
    } else if (dimension === 'mobile') {
        var _iteratorNormalCompletion2 = true;
        var _didIteratorError2 = false;
        var _iteratorError2 = undefined;

        try {

            for (var _iterator2 = house.flats[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                var _flat = _step2.value;


                var $flat = '\n\n            <tr>\n                <td>' + (_flat.rooms !== 0 ? _flat.rooms + ' ком.' : '<span hidden>0</span> Студия') + '</td>\n                <td>' + _flat.floor + '</td>\n                <td>' + _flat.area + ' \u043C<sup>2</sup></td>\n                <td>' + splitDigits(_flat.price) + ' \u0440\u0443\u0431.</td>\n                <td>' + splitDigits(_flat.pricem2) + ' \u0440\u0443\u0431.</td>\n                <td class="td-section--mobile td--mobile">\u042D\u0442\u0430\u0436</td>\n                <td class="td-floor--mobile td--mobile">\u041F\u043B\u043E\u0449\u0430\u0434\u044C</td>\n                <td class="td-area--mobile td--mobile">\u0421\u0442\u043E\u0438\u043C\u043E\u0441\u0442\u044C \u0437\u0430 \u043C<sup>2</sup></td>\n            </tr>\n            ';

                $tableMobile.find('tbody').append($flat);
            }
        } catch (err) {
            _didIteratorError2 = true;
            _iteratorError2 = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion2 && _iterator2.return) {
                    _iterator2.return();
                }
            } finally {
                if (_didIteratorError2) {
                    throw _iteratorError2;
                }
            }
        }
    }
}