'use strict';

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function makeTableSortable(table) {
    var $table = $(table);

    if (!$($table).hasClass('sorting-inited')) {

        try {
            $($table).DataTable(_defineProperty({
                'searching': false,
                'bPaginate': false,
                'bLengthChange': false,
                'bInfo': false,
                "paging": false
            }, 'searching', false));

            $($table).addClass('sorting-inited');
            $($table).find('thead th').click(function () {
                $(this).siblings().removeClass('th--active');
                $(this).toggleClass('th--active');
            });
        } catch (err) {
            console.error(err);
        }
    } else {
        throw new Error('This table is already sortable');
    }
}