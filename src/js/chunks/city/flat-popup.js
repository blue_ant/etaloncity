'use strict';

(function () {

  var flat = $('.flats .flats__container__grid__wrapper__icon:not(.flat--sold):not(.flat--unactive)'),
      popup = $('.flats__popup');

  flat.on('click', function () {

    var flatId = $(this).data('id');

    $.ajax({
      method: "GET",
      url: '/choose/',
      data: { flatId: flatId },
      dataType: 'json'
    }).done(function (data) {

      var parsedData = data;

      var $view = '';

      if (parsedData.viewUrl.length !== 0) {
        parsedData.viewUrl.forEach(function (el, index) {
          $view += '<a ' + (index >= 1 ? 'style="display: none;"' : false) + ' href="' + (el.src ? el.src : false) + '" data-fancybox="' + flat.id + '" data-caption="' + (el.name ? el.name : false) + '" class="flats__view"><div style="background: url(' + (el.src ? el.src : false) + ') no-repeat center; background-size: cover;"><p>\u0412\u0438\u0434 \u0438\u0437 \u043E\u043A\u043D\u0430</p></div></a>';
        });
      }

      // history.pushState({}, 'Выбор квартиры', parsedData.url)

      var filledInputsData = {},
          inputForFilling = $('#form-flat input[name="flat"]'),
          content = '\n          <div class="flats__popup--close"></div>\n\n          <div class="flats__popup__container__left">\n\n            <div class="flats__plan-1">\n              <img src="' + (parsedData.planImg ? parsedData.planImg : '') + '" alt="">\n            </div>\n\n            ' + (parsedData.planSectionImg ? '<div class="flats__plan-2"><small>' + (parsedData.house ? parsedData.house : '') + '</small><img src="' + parsedData.planSectionImg + '" alt=""></div>' : '') + '\n            \n            ' + (parsedData.planFloorImg ? '<div class="flats__plan-3"><small>\u042D\u0442\u0430\u0436</small><img src="' + (parsedData.planFloorImg ? parsedData.planFloorImg : '') + '" alt=""></div>' : '') + '\n\n            <ul>\n              <li>* \u0426\u0435\u043D\u0430 \u0434\u0430\u043D\u0430 \u0441 \u0443\u0447\u0435\u0442\u043E\u043C \u0441\u043A\u0438\u0434\u043E\u043A</li>\n              <li>* \u0420\u0435\u0437\u0435\u0440\u0432 \u043D\u0435 \u0444\u0438\u043A\u0441\u0438\u0440\u0443\u0435\u0442 \u0441\u0442\u043E\u0438\u043C\u043E\u0441\u0442\u044C \u0438 \u0443\u0441\u043B\u043E\u0432\u0438\u044F \u043E\u043F\u043B\u0430\u0442\u044B</li>\n              <li>* \u0426\u0435\u043D\u0430 \u0434\u0435\u0439\u0441\u0442\u0432\u0438\u0442\u0435\u043B\u044C\u043D\u0430 \u043F\u0440\u0438 100% \u043E\u043F\u043B\u0430\u0442\u0435</li>\n            </ul>\n\n          </div>\n          <div class="flats__popup__container__right ' + (parsedData.sectionName && parsedData.section ? 'has-section-name' : '') + ' ' + (parsedData.viewUrl.length !== 0 ? 'has-image' : '') + '">\n            <h3>\u041A\u0432\u0430\u0440\u0442\u0438\u0440\u0430 ' + (parsedData.num ? parsedData.num : '') + '</h3>\n            ' + (parsedData.sectionName && parsedData.section ? '<h4>\u0421\u0435\u043A\u0446\u0438\u044F \u2116' + parsedData.section + ' \xAB' + parsedData.sectionName + '\xBB</h4>' : '') + '\n            <div class="flats__flat-info">\n              <div class="' + (parsedData.floor ? '' : 'hidden') + '">\u042D\u0442\u0430\u0436:<br/><span>' + (parsedData.floor ? parsedData.floor : '') + '</span></div>\n             <div class="' + (parsedData.rooms ? '' : 'hidden') + '">\u041A\u043E\u043C\u043D\u0430\u0442:<br/><span>' + (parsedData.rooms ? parsedData.rooms : '') + (parsedData.rooms == 0 ? 'Студия' : '') + '</span></div>\n             <div class="' + (parsedData.area ? '' : 'hidden') + '">\u041F\u043B\u043E\u0449\u0430\u0434\u044C:<br/><span>' + (parsedData.area ? parsedData.area : '') + ' \u043C<sup>2</sup></span></div>\n            </div>\n            <div class="flats__flat-price">\n              <p>\u0421\u0442\u043E\u0438\u043C\u043E\u0441\u0442\u044C:</p>\n              <p>' + (splitDigits(parsedData.price) ? splitDigits(parsedData.price) : "0") + ' \u0440\u0443\u0431.*</p>\n            </div>\n\n            <div class="flats__buttons-1">\n              ' + $view + '\n              <a href="' + $('.flats__popup').data('furnish-src') + '" class="flats__furniture"><div><p>\u0420\u0430\u0441\u0441\u0442\u0430\u043D\u043E\u0432\u043A\u0430<br/> \u043C\u0435\u0431\u0435\u043B\u0438</p></div></a>\n            </div>\n\n            <div class="flats__buttons-2">\n              <a href="" class="flats__button-apply">\u041E\u0441\u0442\u0430\u0432\u0438\u0442\u044C \u0437\u0430\u044F\u0432\u043A\u0443</a>\n              <a href="' + parsedData.pdfPath + '" target="_blank" class="flats__button-download">\u0421\u043A\u0430\u0447\u0430\u0442\u044C \u043F\u043B\u0430\u043D\u0438\u0440\u043E\u0432\u043A\u0443</a>\n            </div>\n\n            <ul>\n              <li>* \u0426\u0435\u043D\u0430 \u0434\u0430\u043D\u0430 \u0441 \u0443\u0447\u0435\u0442\u043E\u043C \u0441\u043A\u0438\u0434\u043E\u043A</li>\n              <li>* \u0420\u0435\u0437\u0435\u0440\u0432 \u043D\u0435 \u0444\u0438\u043A\u0441\u0438\u0440\u0443\u0435\u0442 \u0441\u0442\u043E\u0438\u043C\u043E\u0441\u0442\u044C \u0438 \u0443\u0441\u043B\u043E\u0432\u0438\u044F \u043E\u043F\u043B\u0430\u0442\u044B</li>\n              <li>* \u0426\u0435\u043D\u0430 \u0434\u0435\u0439\u0441\u0442\u0432\u0438\u0442\u0435\u043B\u044C\u043D\u0430 \u043F\u0440\u0438 100% \u043E\u043F\u043B\u0430\u0442\u0435</li>\n            </ul>\n\n\n          </div>\n        ';

      popup.find('.flats__popup__container').html(content);
      if (checkDevice() !== 'desktop') {
        $('body, html').css('overflow', 'hidden');
        $('body,html').addClass('noscroll');
      }
      $('.flats__popup--close').unbind('click').on('click', function () {
        if (checkDevice() !== 'desktop') {
          $('body, html').removeAttr('style');
          $('body,html').removeClass('noscroll');
        }
        popup.removeClass("flats__popup--visible");
        history.back();
      });

      popup.addClass("flats__popup--visible");

      for (var item in parsedData) {
        if (item === 'house' || item === 'section' || item === 'sectionName' || item === "num") {
          filledInputsData[item] = parsedData[item];
        }
      }
      inputForFilling.val('\u041A\u043E\u0440\u043F\u0443\u0441 \u2116 ' + filledInputsData.house + '; \u0421\u0435\u043A\u0446\u0438\u044F \u2116 ' + filledInputsData.section + '; \u041D\u0430\u0437\u0432\u0430\u043D\u0438\u0435 \u0441\u0435\u043A\u0446\u0438\u0438: ' + filledInputsData.sectionName + '; \u041A\u0432\u0430\u0440\u0442\u0438\u0440\u0430 \u2116 ' + filledInputsData.num);
      fancyboxDefaults();

      $('.flats__popup--close').on('click', function (e) {

        if ($('body').hasClass('flat-mode')) {

          if (document.referrer.match(/choose\/search/g) !== null) {
            document.location = '/choose/search/';
          } else {
            e.preventDefault();
            var url = window.location.href.split('/');
            url.splice(-2);
            url = url.join('/');

            setTimeout(function () {
              window.history.pushState('ChooseFlat', 'Выбор квартиры', url);
            }, 100);
          }
        }
      });
    }).fail(function () {
      alert('Произошла какая-то ошибка. Попробуйте еще раз.');
    });
  });
})();