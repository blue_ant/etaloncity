'use strict';

function completeFillTable(house, tableIndex, dimension) {
    var $tableDesktop = $('.table-desktop[data-table="' + tableIndex + '"]'),
        $tableMobile = $('.table-mobile[data-table="' + tableIndex + '"]');

    if (!house.flats instanceof Array) {
        throw new Error('Flats are not presented as an array');
    } else if (house.flats.length === 0) {
        throw new Error('Flats array is empty');
    }

    if (dimension !== 'mobile') {
        var _iteratorNormalCompletion = true;
        var _didIteratorError = false;
        var _iteratorError = undefined;

        try {
            var _loop = function _loop() {
                var flat = _step.value;


                var $view = '';

                if (flat.viewUrl.length !== 0) {
                    flat.viewUrl.forEach(function (el, index) {
                        $view += '<a class="view-image" ' + (index >= 1 ? 'hidden' : false) + ' href="' + (el.src ? el.src : false) + '" data-fancybox="' + flat.id + '" data-caption="' + (el.name ? el.name : false) + '"><div style="background: url(' + (el.src ? el.src : false) + ') no-repeat center; background-size: cover;"></div></a>';
                    });
                }

                $tableDesktop.DataTable().row.add([$view, '' + (flat.rooms !== 0 ? flat.rooms + ' ком.' : '<span hidden>0</span> Студия'), '' + house.houseName, flat.floor, '' + flat.area, '' + flat.price, '' + flat.pricem2]).draw().node();
                if (flat.discount) {
                    $('tbody tr:last-of-type', $tableDesktop).addClass('has-action');
                    $('tbody tr:last-of-type', $tableDesktop).addClass('flag');
                }
                if (flat.furnish) {
                    $('tbody tr:last-of-type', $tableDesktop).addClass('brush');
                }
                $('<a class="row-link" href="' + flat.url + '"></a>').appendTo($('tbody tr:last-of-type td:last-of-type', $tableDesktop));
            };

            for (var _iterator = house.flats[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                _loop();
            }
        } catch (err) {
            _didIteratorError = true;
            _iteratorError = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion && _iterator.return) {
                    _iterator.return();
                }
            } finally {
                if (_didIteratorError) {
                    throw _iteratorError;
                }
            }
        }
    } else if (dimension === 'mobile') {

        try {
            var _iteratorNormalCompletion2 = true;
            var _didIteratorError2 = false;
            var _iteratorError2 = undefined;

            try {
                var _loop2 = function _loop2() {
                    var flat = _step2.value;


                    var view = '';

                    if (flat.viewUrl.length !== 0) {
                        flat.viewUrl.forEach(function (el, index) {
                            view += '<a ' + (index >= 1 ? 'hidden' : false) + ' href="' + (el.src ? el.src : false) + '" data-fancybox="' + flat.id + '" data-caption="' + (el.name ? el.name : false) + '"><div style="background: url(' + (el.src ? el.src : false) + ') no-repeat center; background-size: cover;"></div></a>';
                        });
                    }

                    var $flat = '\n                \n                <tr>\n                    <td>' + view + '</td>\n                    <td>' + (flat.rooms !== 0 ? flat.rooms + ' ком.' : '<span hidden>0</span> Студия') + '</td>\n                    <td>' + house.houseName + '</td>\n                    <td>' + flat.floor + '</td>\n                    <td>' + flat.area + '</td>\n                    <td>' + splitDigits(flat.price) + ' \u0440\u0443\u0431.</td>\n                    <td class="mobile--hidden"></td>\n                    <td class="td-section--mobile td--mobile">\u0421\u0435\u043A\u0446\u0438\u044F</td>\n                    <td class="td-floor--mobile td--mobile">\u042D\u0442\u0430\u0436</td>\n                    <td class="td-area--mobile td--mobile">\u041F\u043B\u043E\u0449\u0430\u0434\u044C</td>\n                    <td class="td-link--mobile"><a href="' + flat.url + '"><div></div></a></td>\n                </tr>\n                ';

                    $tableMobile.find('tbody').append($flat);
                    $('<a class="row-link" href="' + flat.url + '"></a>').appendTo($('tbody tr:last-of-type td:last-of-type', $tableMobile));
                    if (flat.discount) {
                        $('tbody tr:last-of-type', $tableMobile).addClass('has-action');
                        $('tbody tr:last-of-type', $tableMobile).addClass('flag');
                    }
                    if (flat.furnish) {
                        $('tbody tr:last-of-type', $tableMobile).addClass('brush');
                    }
                };

                for (var _iterator2 = house.flats[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                    _loop2();
                }
            } catch (err) {
                _didIteratorError2 = true;
                _iteratorError2 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion2 && _iterator2.return) {
                        _iterator2.return();
                    }
                } finally {
                    if (_didIteratorError2) {
                        throw _iteratorError2;
                    }
                }
            }
        } catch (err) {
            console.error(err);
        }
    }
}