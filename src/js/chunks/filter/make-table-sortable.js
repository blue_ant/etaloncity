'use strict';

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function splitDigits(num) {
    var str = num.toString().split('.');
    if (str[0].length >= 5) {
        str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1 ');
    }
    if (str[1] && str[1].length >= 5) {
        str[1] = str[1].replace(/(\d{3})/g, '$1 ');
    }
    return str.join('.');
}

function makeTableSortable(table) {
    var $table = $(table);

    if (!$($table).hasClass('sorting-inited')) {

        try {
            var _$$DataTable;

            $($table).DataTable((_$$DataTable = {
                'searching': false,
                'bPaginate': false,
                'bLengthChange': false,
                'bInfo': false,
                "paging": false
            }, _defineProperty(_$$DataTable, 'searching', false), _defineProperty(_$$DataTable, 'createdRow', function createdRow(row, data, dataIndex) {

                $(row).find('td').eq(4).html(data[4] + ' м<sup>2</sup>');
                $(row).find('td').eq(5).html(splitDigits(parseInt(data[5], 10)) + ' руб.');
                $(row).find('td').eq(6).html(splitDigits(parseInt(data[6], 10)) + ' руб.');
                // $(row).find('td').last().append('<a href="#">fff</a>')
            }), _$$DataTable));

            $($table).addClass('sorting-inited');
            $($table).find('thead th').click(function () {
                $(this).siblings().removeClass('th--active');
                $(this).toggleClass('th--active');
            });
        } catch (err) {
            console.error(err);
        }
    } else {
        throw new Error('This table is already sortable');
    }
}