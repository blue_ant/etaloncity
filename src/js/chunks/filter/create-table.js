'use strict';

function createTable(house) {
    var index = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'none';
    var dimension = arguments[2];

    var $table = void 0;

    if (dimension !== 'mobile') {

        $table = '\n\n            <table data-table="' + index + '" class="table table-desktop">\n                <thead class="thead">\n                    <th data-sort="integer" class="table-th">' + house.houseName + '</th>\n                    <th data-sort="integer" class="table-th" class="th--active">\u0422\u0438\u043F</th>\n                    <th data-sort="integer" class="table-th">\u0421\u0435\u043A\u0446\u0438\u044F</th>\n                    <th data-sort="integer" class="table-th">\u042D\u0442\u0430\u0436</th>\n                    <th data-sort="integer" class="table-th">\u041F\u043B\u043E\u0449\u0430\u0434\u044C</th>\n                    <th data-sort="integer" class="table-th">\u0426\u0435\u043D\u0430</th>\n                    <th data-sort="integer" class="table-th">\u0421\u0442\u043E\u0438\u043C\u043E\u0441\u0442\u044C \u0437\u0430 \u043C<sup>2</sup></th>\n                </thead>\n                <tbody></tbody>\n            </table>\n        ';
        // <th class="table-th">Планировка</th>

        if (house.flats.length > 4) {
            $table += '<a href="" role="button" data-table-button="' + index + '" data-step="1" class="show-more show-more--desktop">\u041F\u043E\u043A\u0430\u0437\u0430\u0442\u044C \u0432\u0441\u0435 \u043A\u0432\u0430\u0440\u0442\u0438\u0440\u044B</a>';
        }
    } else {

        $table = '\n\n            <table data-table="' + index + '" class="table table-mobile">\n                <thead class="thead">\n                    <th class="table-th">' + house.houseName + '</th>\n                    <th class="table-th" class="th--active">\u0422\u0438\u043F</th>\n                    <th class="table-th">\u0421\u0435\u043A\u0446\u0438\u044F</th>\n                    <th class="table-th">\u042D\u0442\u0430\u0436</th>\n                    <th class="table-th">\u041F\u043B\u043E\u0449\u0430\u0434\u044C</th>\n                    <th class="table-th">\u0426\u0435\u043D\u0430</th>\n                    <th class="table-th">\u0421\u0442\u043E\u0438\u043C\u043E\u0441\u0442\u044C \u0437\u0430 \u043C<sup>2</sup></th>\n                    <th class="table-th table-th-mobile">\u0421\u0432\u043E\u0431\u043E\u0434\u043D\u043E ' + house.total + ' \u043A\u0432.</th>\n                </thead>\n                <tbody></tbody>\n            </table>\n        ';

        if (house.flats.length > 4) {
            $table += '<a href="" role="button" data-table-button="' + index + '" data-step="1" class="show-more show-more--mobile">\u041F\u043E\u043A\u0430\u0437\u0430\u0442\u044C \u0432\u0441\u0435 \u043A\u0432\u0430\u0440\u0442\u0438\u0440\u044B</a>';
        }
    }

    return $table;
}