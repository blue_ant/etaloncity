'use strict';

(function () {
	var _loop = function _loop(i, len) {

		$('.sidebar__slider-' + i + '__inputs input').on('change', function () {
			var val = [],
			    checkboxOne = $('.sidebar__slider-' + i + '__inputs input:nth-of-type(1)'),
			    checkboxTwo = $('.sidebar__slider-' + i + '__inputs input:nth-of-type(2)');

			if ($(this).prop('name') == checkboxOne.prop('name')) {
				val.push($(this).val());
				val.push(checkboxTwo.val());
			} else if ($(this).prop("name") == checkboxTwo.prop('name')) {
				val.push(checkboxOne.val());
				val.push($(this).val());
			}

			store.set('slider-' + i, val);
		});

		$('#slider-' + i).on('slidestop', function (event, ui) {
			var val = ui.values;
			store.set('slider-' + i, val);
		});
	};

	// Sliderrs
	for (var i = 1, len = $('[class*="sidebar__slider-"][data-min][data-max]').length; i <= len; i++) {
		_loop(i, len);
	}

	// Rooms

	var _loop2 = function _loop2(i, len) {
		$('#check-' + i).on('change', function () {
			var val = $(this).is(':checked');
			store.set('room-' + (i - 1), val);
		});
	};

	for (var i = 1, len = $('.sidebar__room__check input').length; i <= len; i++) {
		_loop2(i, len);
	}

	// Options

	var _loop3 = function _loop3(i, len) {
		$('#checkbox-' + i).on('change', function () {
			var val = $(this).is(':checked');
			store.set('option-' + i, val);
		});
	};

	for (var i = 1, len = $('.sidebar__option input').length; i <= len; i++) {
		_loop3(i, len);
	}
})();