'use strict';

$(document).ready(function () {

	(function () {

		var triggers = [$('[href="#fragment-1"]'), $('[href="#fragment-2"]'), $('[href="#fragment-3"]')];
	})();

	(function () {
		$('#owl-carousel').owlCarousel({
			dots: false,
			nav: true,
			items: 1,
			navText: false,
			touchDrag: true,
			mouseDrag: true,
			loop: true
		});
	})();

	// Switch sections
	(function () {
		var trigger = $('.estate_commercial [data-switch-to]'),
		    sections = [$('#fragment-1'), $('#fragment-2'), $('#fragment-3')];

		trigger.on('click', function (e) {
			e.preventDefault();
			if ($(this).data('switch-to') == '2') {
				sections.forEach(function (item, index) {
					item.removeClass('hidden');
					if (index == '1') {
						item.removeClass('hidden');
					} else {
						item.addClass('hidden');
					}
				});
			} else if ($(this).data('switch-to') == '3') {

				sections.forEach(function (item, index) {
					if (index == '2') {
						item.removeClass('hidden');
					} else {
						item.addClass('hidden');
					}
				});
			} else if ($(this).data('switch-to') == '1') {

				sections.forEach(function (item, index) {
					if (index == '0') {
						item.removeClass('hidden');
					} else {
						item.addClass('hidden');
					}
				});
			}
		});
	})();

	(function () {

		var parentEl = $('#fragment-1'),
		    flat = $('.flat-link'),
		    linkBack = $('.link-back'),
		    flatView = [$('#fragment-1_1'), $('#fragment-1_2')];

		flat.on('click', function (e) {
			e.preventDefault();
			if ($(this).data('switch-to') == 'flat-1') {
				parentEl.addClass('hidden');
				flatView[0].removeClass('hidden');
			} else if ($(this).data('switch-to') == 'flat-2') {
				parentEl.addClass('hidden');
				flatView[1].removeClass('hidden');
			}
		});

		linkBack.on('click', function () {
			parentEl.removeClass('hidden');
			flatView.forEach(function (el, index) {
				el.addClass('hidden');
			});
		});
	})();
});