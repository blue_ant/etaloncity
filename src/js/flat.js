"use strict";

$(document).ready(function () {
  var checkDevice = function checkDevice(options) {
    var result = void 0,
        ww = $(window).width(),
        current = {
      desktop: 1239,
      mobile: 767
    };

    if (options !== 'undefined') {

      for (var item in options) {
        current[item] = item;
      }
    }

    if (ww > current.desktop) {
      result = "desktop";
    } else if (ww < current.desktop && ww > current.mobile) {
      result = "tablet";
    } else if (ww < current.mobile) {
      result = "mobile";
    }

    return result;
  };

  (function () {

    var mySwiper = new Swiper('.swiper-container', {
      effect: 'fade',
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
      },
      on: {
        slideChangeTransitionStart: function slideChangeTransitionStart() {

          $(".flat-content__counter span").html(this.activeIndex + 1);
        },
        init: function init() {

          $(".flat-content__counter span").html('1');
          $(".flat-content__counter small").html("/ " + this.slides.length);
        }
      }
    });
  })();

  (function () {
    var footer = $('footer.footer');

    footer.css({
      'bottom': '-' + $('footer.footer').height() + 'px'
    });

    $(window).resize(function () {
      footer.css({
        'bottom': '-' + $('footer.footer').height() + 'px'
      });
    });
  })();
});