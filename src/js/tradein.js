"use strict";

$(document).ready(function () {

	var checkDevice = function checkDevice(options) {

		var result = void 0,
		    ww = $(window).width(),
		    current = {

			desktop: 1239,

			mobile: 767

		};

		if (options !== 'undefined') {

			for (var item in options) {

				current[item] = item;
			}
		}

		if (ww > current.desktop) {

			result = "desktop";
		} else if (ww < current.desktop && ww > current.mobile) {

			result = "tablet";
		} else if (ww < current.mobile) {

			result = "mobile";
		}

		return result;
	};

	(function () {
		$('#owl-carousel').owlCarousel({
			dots: false,
			nav: true,
			items: 1,
			navText: false,
			touchDrag: true,
			mouseDrag: true,
			loop: true
		});
	})();
});