'use strict';

const $document = $(document);

$(function() {
    var open   = '_open';
    var $popup = $('[data-form-popup]');
    var $parking = $('.parking');

    $('[data-select]').each(function() {
        let $select = $(this);

        $select.selectric({
            inheritOriginalWidth: false
        });
    });

    $('[data-parking-link]').on('click', function(e) {
        e.preventDefault();

        let href = $(this).attr('href');

        $popup.show().stop().animate({
            opacity: 1
        }, function() {
            $(href).addClass(open);
            $parking.addClass(open);
        });
    });

    $document.on('click', function(e) {
        let $target = $(e.target);

        if ($target.closest('.parking-form').length || $target.is('[data-parking-link]')) return;

        $('.parking-form').removeClass(open);

        $popup.stop().animate({
            opacity: 0
        }, function() {
            $(this).hide();
            $parking.removeClass(open);
        });
    });

    function validateNumber(event) {
        var key = window.event ? event.keyCode : event.which;
        if (event.keyCode === 8 || event.keyCode === 46) {
            return true;
        } else if ( key < 48 || key > 57 ) {
            return false;
        } else {
            return true;
        }
    };

    $('[data-summ]').on('keypress', validateNumber);

    $('[data-summ]').on('input', function() {
        let $this = $(this);
        let val   = Number($this.val().replace(/\D/g,'').substring(0,8));

        if (val === '') return;

        $this.val(Number(val).toLocaleString("ru-RU"));
    });

    $('#payment-form').on('submit', function(e) {
        e.preventDefault();

        let $form  = $(this);
        let url    = $form.attr('action');
        let method = $form.attr('method');
        let data   = $form.serialize();

        $.ajax({
            url: url,
            method: method,
            data: data,
            success: function(data) {
                console.log(data);
            }
        })
    });

    // Инициализация форм
    webshim.setOptions('forms', {
        lazyCustomMessages: true,
        addValidators: true,
    });

    //start polyfilling
    webshim.polyfill('forms');
});
