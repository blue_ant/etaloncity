$(document).ready(function() {

    // =include common/headerClass-by-device.js
    // =include common/check-device.js
    // =include common/split-digits.js

    let dimension = checkDevice();

    // =include chunks/filter/complete-full-table.js
    // =include chunks/filter/create-table.js
    // =include chunks/filter/init-fill-table.js
    // =include chunks/filter/insert-table.js
    // =include chunks/filter/make-table-sortable.js


    (() => {
        let sliderWrappers = [$('.sidebar__slider-1'), $('.sidebar__slider-2'), $('.sidebar__slider-3'), $('.sidebar__slider-4')],
            sliderRangeNumWrappers = [$('.sidebar__slider-1__values'), $('.sidebar__slider-2__values'), $('.sidebar__slider-3__values'), $('.sidebar__slider-4__values')];

        let sliderStartValues = [{
                min: sliderWrappers[0].data('min'),
                max: sliderWrappers[0].data('max')
            },
            {
                min: sliderWrappers[1].data('min'),
                max: sliderWrappers[1].data('max')
            },
            {
                min: sliderWrappers[2].data('min'),
                max: sliderWrappers[2].data('max')
            },
            {
                min: sliderWrappers[3].data('min'),
                max: sliderWrappers[3].data('max')
            }
        ];

        let rangeValues = [
            [
                sliderStartValues[0].min,
                parseInt((((sliderStartValues[0].min + sliderStartValues[0].max) / 2) + sliderStartValues[0].min) / 2, 10),
                parseInt((sliderStartValues[0].min + sliderStartValues[0].max) / 2, 10),
                parseInt((((sliderStartValues[0].min + sliderStartValues[0].max) / 2) + sliderStartValues[0].max) / 2, 10),
                sliderStartValues[0].max
            ],
            [
                sliderStartValues[1].min,
                parseInt((((sliderStartValues[1].min + sliderStartValues[1].max) / 2) + sliderStartValues[1].min) / 2, 10),
                parseInt((sliderStartValues[1].min + sliderStartValues[1].max) / 2, 10),
                parseInt((((sliderStartValues[1].min + sliderStartValues[1].max) / 2) + sliderStartValues[1].max) / 2, 10),
                sliderStartValues[1].max
            ],
            [
                sliderStartValues[2].min,
                parseInt((((sliderStartValues[2].min + sliderStartValues[2].max) / 2) + sliderStartValues[2].min) / 2, 10),
                parseInt((sliderStartValues[2].min + sliderStartValues[2].max) / 2, 10),
                parseInt((((sliderStartValues[2].min + sliderStartValues[2].max) / 2) + sliderStartValues[2].max) / 2, 10),
                sliderStartValues[2].max
            ],
            [
                sliderStartValues[3].min,
                parseInt((((sliderStartValues[3].min + sliderStartValues[3].max) / 2) + sliderStartValues[3].min) / 2, 10),
                parseInt((sliderStartValues[3].min + sliderStartValues[3].max) / 2, 10),
                parseInt((((sliderStartValues[3].min + sliderStartValues[3].max) / 2) + sliderStartValues[3].max) / 2, 10),
                sliderStartValues[3].max
            ]
        ];

        if (sliderWrappers[0] && rangeValues[0][0]) {
            try {
                sliderRangeNumWrappers.forEach(function(el, index) {

                    for (let i = 0; i < 5; i++) {
                        let small = document.createElement('small');
                        small.innerHTML = rangeValues[index][i] + '';
                        el.append(small);
                    }
                });
            } catch (err) {
                console.error(err);
            }
        }

    })();


    // Initialization of sliders in sidebar
    (() => {

        var one = $('#slider-1'),
            two = $('#slider-2'),
            three = $('#slider-3'),
            four = $('#slider-4');

        if (one) {

            let val = [$('.sidebar__slider-1').data('min'), $('.sidebar__slider-1').data('max')];

            $('[name="price-from"]').val(val[0]);
            $('[name="price-to"]').val(val[1]);

            one.slider({
                min: val[0],
                max: val[1],
                range: true,
                values: [val[0], val[1]],
                slide: function(event, ui) {
                    $('.sidebar__slider-1__inputs input:first-of-type').val(ui.values[0]);
                    $('.sidebar__slider-1__inputs input:last-of-type').val(ui.values[1]);
                },
                stop: function(event, ui) {
                    getFlats();
                }
            });
        }

        if (two) {

            let val = [$('.sidebar__slider-2').data('min'), $('.sidebar__slider-2').data('max')];

            $('[name="price-meter-from"]').val(val[0]);
            $('[name="price-meter-to"]').val(val[1]);

            two.slider({
                min: val[0],
                max: val[1],
                range: true,
                values: [val[0], val[1]],
                slide: function(event, ui) {
                    $('.sidebar__slider-2__inputs input:first-of-type').val(ui.values[0]);
                    $('.sidebar__slider-2__inputs input:last-of-type').val(ui.values[1]);
                },
                stop: function() {
                    getFlats();
                }
            });
        }

        if (three) {

            let val = [$('.sidebar__slider-3').data('min'), $('.sidebar__slider-3').data('max')];

            $('[name="floor-from"]').val(val[0]);
            $('[name="floor-to"]').val(val[1]);

            three.slider({
                min: val[0],
                max: val[1],
                range: true,
                values: [val[0], val[1]],
                slide: function(event, ui) {
                    $('.sidebar__slider-3__inputs input:first-of-type').val(ui.values[0]);
                    $('.sidebar__slider-3__inputs input:last-of-type').val(ui.values[1]);
                },
                stop: function() {
                    getFlats();
                }
            });
        }

        if (four) {

            let val = [$('.sidebar__slider-4').data('min'), $('.sidebar__slider-4').data('max')];

            $('[name="sq-from"]').val(val[0]);
            $('[name="sq-to"]').val(val[1]);

            four.slider({
                min: val[0],
                max: val[1],
                range: true,
                values: [val[0], val[1]],
                slide: function(event, ui) {
                    $('.sidebar__slider-4__inputs input:first-of-type').val(ui.values[0]);
                    $('.sidebar__slider-4__inputs input:last-of-type').val(ui.values[1]);
                },
                stop: function() {
                    getFlats();
                }
            });
        }

        $('aside.sidebar-filter input').on('change', function() {
            getFlats();
            one.slider({values: [$('[name="price-from"]').val(), $('[name="price-to"]').val()]});
            two.slider({values: [$('[name="price-meter-from"]').val(), $('[name="price-meter-to"]').val()]});
            three.slider({values: [$('[name="floor-from"]').val(), $('[name="floor-to"]').val()]});
            four.slider({values: [$('[name="sq-from"]').val(), $('[name="sq-to"]').val()]});
        });


    })();

    // LIGHTBOX—————————————————————————
  function fancyboxDefaults() {

    let elems = $("[data-fancybox]");

        $.extend($.fancybox.defaults, {
            animationEffect: "zoom-in-out",
            touch: false,
            idleTime: 0,
            margin: [40, 40],
            buttons: ['close'],
            afterLoad: function() {
                let imgWrapper = $('.fancybox-container .fancybox-image-wrap'),
                    el = $('.fancybox-container .fancybox-caption-wrap');

                imgWrapper.prepend(el);
            },
            afterShow: function() {
                let imgWrapper = $('.fancybox-container .fancybox-image-wrap'),
                    el = $('.fancybox-container .fancybox-navigation');

                el.addClass('fancybox-navigation-visible');
                $('.fancybox-container .fancybox-caption-wrap').addClass('fancybox-caption-wrap--show');
                $('.fancybox-container .fancybox-navigation .count-wrapper').remove();
                $('.fancybox-container .fancybox-navigation').prepend(`<div class="count-wrapper"><span class="image-index">${this.index + 1}</span>/<span class="image-total"> / ${$('.fancybox-image').length}</span></div>`);
            },
            beforeClose: function() {
                $('.fancybox-navigation-visible').removeClass('fancybox-navigation-visible');
            }
        });
  }
    //——————————————————————————————


    // TABLE SORTING———————————————————————

    function moreFlats(houseNum, offset) {

        function getRooms() {
            let arr = [];

            $('aside.sidebar #check-1').is(':checked') ? arr.push(0) : false;
            $('aside.sidebar #check-2').is(':checked') ? arr.push(1) : false;
            $('aside.sidebar #check-3').is(':checked') ? arr.push(2) : false;
            $('aside.sidebar #check-4').is(':checked') ? arr.push(3) : false;

            return arr;
        }

        let house,
            offsetReady = offset,
            loader = $('.loader'),
            foundSomething = false,
            $tablesWrapper = $('.filter .container'),
            opts = {
                get_flats: 'json',
                limit: 10,
                offset: offsetReady,
                house: houseNum,
                priceFrom: $('aside.sidebar [name="price-from"]').val(),
                priceTo: $('aside.sidebar [name="price-to"]').val(),
                priceMeterFrom: $('aside.sidebar [name="price-meter-from"]').val(),
                priceMeterTo: $('aside.sidebar [name="price-meter-to"]').val(),
                floorFrom: $('aside.sidebar [name="floor-from"]').val(),
                floorTo: $('aside.sidebar [name="floor-to"]').val(),
                rooms: getRooms(), //массив
                sqFrom: $('aside.sidebar [name="sq-from"]').val(),
                sqTo: $('aside.sidebar [name="sq-to"]').val(),
                furnish: `${$('aside.sidebar #checkbox-1').is(':checked') ? '1' : '0'}`,
                flatview: `${$('aside.sidebar #checkbox-2').is(':checked') ? '1' : '0'}`,
                discount: `${$('aside.sidebar #checkbox-3').is(':checked') ? '1' : '0'}`
            };

        $.ajax({
            method: "GET",
            url: '/choose/search/',
            data: opts,
            dataType: 'json',
            beforeSend: function() {
                loader.addClass('loader--active');
            }   
        }).done((response) => {
            console.log(response)
            try {
                house = response.houses[0];

            }
            catch(err) {
                console.error(err);
            }

            if (house.flats.length === 0) {
                $(`.show-more[data-table-button="${houseNum - 1}"]`).remove();
                loader.removeClass('loader--active');
                return;
            }

            completeFillTable(house, houseNum - 1, dimension);
            loader.removeClass('loader--active');

            if(house.flats.length < 10) {
                $(`.show-more[data-table-button="${houseNum - 1}"]`).remove();
            }

            fancyboxDefaults();
        });

    }

    function getFlats() {

    let houses = [],
        foundSomething = false,
        $tablesWrapper = $('.filter .container'),
        loader = $('.loader');

        function getRooms() {
            let arr = [];

            $('aside.sidebar #check-1').is(':checked') ? arr.push(0) : false;
            $('aside.sidebar #check-2').is(':checked') ? arr.push(1) : false;
            $('aside.sidebar #check-3').is(':checked') ? arr.push(2) : false;
            $('aside.sidebar #check-4').is(':checked') ? arr.push(3) : false;

            return arr;
        }

        let sidebar = $("aside.sidebar"),
            opts = {
                get_flats: 'json',
                priceFrom: $('aside.sidebar [name="price-from"]').val(),
                priceTo: $('aside.sidebar [name="price-to"]').val(),
                priceMeterFrom: $('aside.sidebar [name="price-meter-from"]').val(),
                priceMeterTo: $('aside.sidebar [name="price-meter-to"]').val(),
                floorFrom: $('aside.sidebar [name="floor-from"]').val(),
                floorTo: $('aside.sidebar [name="floor-to"]').val(),
                rooms: getRooms(), //массив
                sqFrom: $('aside.sidebar [name="sq-from"]').val(),
                sqTo: $('aside.sidebar [name="sq-to"]').val(),
                furnish: `${$('aside.sidebar #checkbox-1').is(':checked') ? '1' : '0'}`,
                flatview: `${$('aside.sidebar #checkbox-2').is(':checked') ? '1' : '0'}`,
                discount: `${$('aside.sidebar #checkbox-3').is(':checked') ? '1' : '0'}`
            };

        $.ajax({
            method: "GET",
            url: '/choose/search/',
            data: opts,
            dataType: 'json',
            beforeSend: function() {
                $tablesWrapper.empty();
                $tablesWrapper.prepend('<h2>Поиск квартир</h2>');
                loader.addClass('loader--active');
            }
        }).done((response) => {
            console.log(response)
            try {
                houses = response.houses;

                for(let house of houses) {
                    if (house.flats.length !== 0) foundSomething = true;
                }
            }
            catch(err) {
                console.error(err);
            }


            if(foundSomething) {
                $tablesWrapper.empty();
                $tablesWrapper.prepend('<h2>Поиск квартир</h2>');
            }
            else {
                $tablesWrapper.empty();
                $tablesWrapper.prepend('<h2 class="h2-no-result">Квартир не найдено. <br/>Измените параметры <br/>поиска.</h2>');
                loader.removeClass('loader--active');
            }
            //Создание и вставка таблиц в DOM
            houses.forEach(function(item, index) {
                if(item.flats.length === 0) return;

                let $table = createTable(item, index, dimension); // Создание таблицы

                insertTable($table,$tablesWrapper); // Вставка таблицы в DOM
                makeTableSortable($(`.table-desktop[data-table="${index}"]`)); // Инициализация сортируемости таблицы
                initFillTable(item, $(`.table-desktop[data-table="${index}"]`), index);
                loader.removeClass('loader--active');

				$(`.show-more[data-table-button="${index}"]`).unbind('click').click(function(event) {
                    let offset = $(`.table[data-table="${index}"] tbody tr`).get().length;
                    event.preventDefault();
                    moreFlats(index + 1, offset);
                    offset += 10;
				});
            });
            $('.row-link').click(function() {
                let lastSeenSection = $(this).closest('table').data('table');

                store.set(`lastSeenSection`, +lastSeenSection);
            });
            if (store.get('lastSeenSection') !== null) {
                $('html').animate({
                    'scrollTop': $(`table.table[data-table="${store.get('lastSeenSection')}"]`).offset().top + 'px'
                }, 350, function() {
                    store.remove('lastSeenSection');
                });
            }
            fancyboxDefaults();
        }).fail((err) => {
            throw err;
        });
    }

    function styleScrollbar() {
        var elem = $('.sidebar .sidebar__wrapper');
        var elemTable = $('.sidebar .table-wrapper');

        if ($(window).width() < 1023) {
            elem.niceScroll({
                cursorcolor: "#F8B00A",
                cursorfixedheight: 0,
                cursoropacitymin: 0,
                cursorwidth: 0,
                preventmultitouchscrolling: false,
                touchbehavior: true,
                horizrailenabled: true,
                cursorborder: '0px solid transparent',
            });

            elemTable.niceScroll({
                cursorcolor: "#F8B00A",
                cursorfixedheight: 60,
                cursoropacitymin: 1,
                cursorborder: '0px solid transparent',
                preventmultitouchscrolling: false,
                touchbehavior: true,
                horizrailenabled: true,
                railoffset: {
                    left: 23,
                    top: 0
                }
            });
        } else {
            elem.niceScroll({
                cursorcolor: "#F8B00A",
                cursorfixedheight: 0,
                cursoropacitymin: 0,
                cursorwidth: 0,
                preventmultitouchscrolling: false,
                horizrailenabled: true,
                cursorborder: '0px solid transparent',
            });

            elemTable.niceScroll({
                cursorcolor: "#F8B00A",
                cursorfixedheight: 60,
                cursoropacitymin: 1,
                cursorborder: '0px solid transparent',
                horizrailenabled: true,
                preventmultitouchscrolling: false,
                railoffset: {
                    left: 23,
                    top: 0
                }
            });
        }

    }
    styleScrollbar();

    (() => {
        $('.sidebar-trigger').on('click', function() {
            var $trigger = $(this),
                $sidebar = $('aside.sidebar'),
                $filter = $('.filter'),
                $footer = $('.footer'),
                $loader = $('.loader');

            $sidebar.toggleClass('sidebar--active');
            $filter.toggleClass('sidebar-filter--active');
            $footer.toggleClass('sidebar-filter--active');
            $loader.toggleClass('loader-filter--active');

        })

    })();

    // =include chunks/filter/filter-preferences.js



    // Apply old filter preferences
    function applyFilterPref () {

        try {

            // Sliderrs
            for(let i = 1, len = $('[class*="sidebar__slider-"][data-min][data-max]').length; i <= len; i++) {

                let initialValue = store.get(`slider-${i}`);

                if (store.get(`slider-${i}`) !== null) {
 
                    $(`#slider-${i}`).slider({values: initialValue});
                    $(`.sidebar__slider-${i} .sidebar__slider-${i}__inputs input:first-of-type`).val(initialValue[0]);
                    $(`.sidebar__slider-${i} .sidebar__slider-${i}__inputs input:last-of-type`).val(initialValue[1]);
                }
            }


            // Rooms
            for (let i = 1, len = $('.sidebar__room__check input').length; i <= len; i++) {
                
                if (store.get(`room-${i - 1}`) !== null) {
                    if (store.get(`room-${i - 1}`) === true) {
                        $(`#check-${i}`).prop('checked', true);
                    }
                    else {
                        $(`#check-${i}`).prop('checked', false);
                    }
                }

            }

            // Options
            for (let i = 1, len = $('.sidebar__option input').length; i <= len; i++) {


                if (store.get(`option-${i}`) !== null) {
                    if (store.get(`option-${i}`) === true) {
                        $(`#checkbox-${i}`).prop('checked', true);
                    }
                    else {
                        $(`#checkbox-${i}`).prop('checked', false);
                    }
                }
            }
        }
        catch(e) {
            console.error(e);
        }


    }


    // INIT ———————————————————————
    (() => {
        applyFilterPref();
        getFlats();
    })();
    // ————————————————————————————

});