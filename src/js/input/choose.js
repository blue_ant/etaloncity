$(document).ready(function() {

    // =include common/check-device.js
    // =include common/split-digits.js

    // =include chunks/choose/*.js




    (function() {

        let links = $('[data-mobile-src]');

        links.on('click', function(e) {

            if (checkDevice() !== 'desktop') {

                e.preventDefault();

                let urlTo = $(this).data('mobile-src');
                
                document.location.href = urlTo;
            }
            else {
                return;
            }
        });

    })();



    // Initializing custom scrollbar for tables in sidebar

    function customSidebarScroll() {

        let elem = $('.sidebar .sidebar__wrapper');

        if (elem) {
            if ($(window).width() < 1023) {
            	 elem.getNiceScroll().remove();

                elem.niceScroll({
                    cursorcolor: "gray",
                    cursorfixedheight: 100,
                    cursoropacitymin: 0.2,
                    cursoropacitymin: 0.2,
                    cursorwidth: 4,
                    preventmultitouchscrolling: false,
                    horizrailenabled: true,
                    cursorborder: '0px solid transparent',
                    railoffset: {
                        left: 15,
                        top: 0,
                        right: 0,
                        bottom: 0
                    }
                });

            } else {
            	 elem.getNiceScroll().remove();

                elem.niceScroll({
                    cursorcolor: "gray",
                    cursorfixedheight: 100,
                    cursoropacitymin: 0.2,
                    cursoropacitymin: 0.2,
                    cursorwidth: 4,
                    preventmultitouchscrolling: false,
                    horizrailenabled: true,
                    cursorborder: '0px solid transparent',
                    railoffset: {
                        left: 15,
                        top: 0,
                        right: 0,
                        bottom: 0
                    }
                });

            }
        }
    }


    // Adding value range for sliders in sidebar
    (() => {
        let sliderWrappers = [$('.sidebar__slider-1'), $('.sidebar__slider-2'), $('.sidebar__slider-3')],
            sliderRangeNumWrappers = [$('.sidebar__slider-1__values'), $('.sidebar__slider-2__values'), $('.sidebar__slider-3__values')];

        let sliderStartValues = [{
                min: sliderWrappers[0].data('min'),
                max: sliderWrappers[0].data('max')
            },
            {
                min: sliderWrappers[1].data('min'),
                max: sliderWrappers[1].data('max')
            },
            {
                min: sliderWrappers[2].data('min'),
                max: sliderWrappers[2].data('max')
            }
        ];

        let rangeValues = [
            [
                sliderStartValues[0].min,
                parseInt((((sliderStartValues[0].min + sliderStartValues[0].max) / 2) + sliderStartValues[0].min) / 2, 10),
                parseInt((sliderStartValues[0].min + sliderStartValues[0].max) / 2, 10),
                parseInt((((sliderStartValues[0].min + sliderStartValues[0].max) / 2) + sliderStartValues[0].max) / 2, 10),
                sliderStartValues[0].max
            ],
            [
                sliderStartValues[1].min,
                parseInt((((sliderStartValues[1].min + sliderStartValues[1].max) / 2) + sliderStartValues[1].min) / 2, 10),
                parseInt((sliderStartValues[1].min + sliderStartValues[1].max) / 2, 10),
                parseInt((((sliderStartValues[1].min + sliderStartValues[1].max) / 2) + sliderStartValues[1].max) / 2, 10),
                sliderStartValues[1].max
            ],
            [
                sliderStartValues[2].min,
                parseInt((((sliderStartValues[2].min + sliderStartValues[2].max) / 2) + sliderStartValues[2].min) / 2, 10),
                parseInt((sliderStartValues[2].min + sliderStartValues[2].max) / 2, 10),
                parseInt((((sliderStartValues[2].min + sliderStartValues[2].max) / 2) + sliderStartValues[2].max) / 2, 10),
                sliderStartValues[2].max
            ]
        ];

        if (sliderWrappers[0] && rangeValues[0][0]) {
            try {
                sliderRangeNumWrappers.forEach(function(el, index) {

                    for (let i = 0; i < 5; i++) {
                        let small = document.createElement('small');
                        small.innerHTML = splitDigits(rangeValues[index][i] + '');
                        el.append(small);
                    }
                });
            } catch (err) {
                console.error(err);
            }
        }

    })();


    // Toggle sidebar trigger

    (() => {
        var button = $('.sidebar-trigger');
        var sidebar = $('aside.sidebar');

        if (typeof button !== 'undefined' && typeof sidebar !== 'undefined') {
            button.on('click', () => {

                button.toggleClass('sidebar-trigger--active');
                sidebar.toggleClass('sidebar--active');
            });
        }
    })();


    // Redirect to filled towers

    (() => {
        let towers = $('.pin_tower').closest('a');
        let hrefs = [];

        for(let item of towers) {
            hrefs.push($(item).attr('href'));
        }
        hrefs.sort();


        towers.on('click', function(e) {
            if ($(this).hasClass('has-no-flats')) {
                e.preventDefault();
                let self = $(this),
                    num = parseInt($(this).text().trim().slice(-1), 10),
                    leadToLink;

                switch(num) {
                    case 1 : leadToLink = hrefs[1];
                    break;
                    case 2 : leadToLink = hrefs[2];
                    break;
                    case 3 : leadToLink = hrefs[3];
                    break;
                    case 4 : leadToLink = hrefs[4];
                    break;
                    case 5 : leadToLink = hrefs[5];
                    break;
                    case 6 : leadToLink = hrefs[0];
                    break;
                }

                document.location.pathname = leadToLink;
            }
            else {
                return;
            }

        });


    })();


    // Initialization of modals with panorama

    (() => {

        let modals = [$("#modal-1-init"), $("#modal-2-init"), $("#modal-3-init"), $("#modal-4-init"), $("#modal-5-init"), $("#modal-6-init"), $("#modal-7-init")];

        if (modals[0]) {

            modals[0].animatedModal({
                modalTarget: 'modal-1',
                animatedIn: 'bounceInUp',
                animatedOut: 'zoomOut',
                color: 'white'
            });
        }

        if (modals[1]) {

            modals[1].animatedModal({
                modalTarget: 'modal-2',
                animatedIn: 'bounceInUp',
                animatedOut: 'zoomOut',
                color: 'white',
                beforeOpen: function() {
                    if ($('#panorama-2').is(':empty')) {
                        pannellum.viewer('panorama-2', {
                            "type": "equirectangular",
                            "panorama": "/img/choose/cam-2.jpg",
                            'autoLoad': true,
                            'showControls': false
                        });
                    }
                }
            });
        }


        if (modals[2]) {

            modals[2].animatedModal({
                modalTarget: 'modal-3',
                animatedIn: 'bounceInUp',
                animatedOut: 'zoomOut',
                color: 'white',
                beforeOpen: function() {
                    if ($('#panorama-3').is(':empty')) {
                        pannellum.viewer('panorama-3', {
                            "type": "equirectangular",
                            "panorama": "/img/choose/cam-3.jpg",
                            'autoLoad': true,
                            'showControls': false
                        });
                    }
                }
            });
        }

        if (modals[3]) {

            modals[3].animatedModal({
                modalTarget: 'modal-4',
                animatedIn: 'bounceInUp',
                animatedOut: 'zoomOut',
                color: 'white',
                beforeOpen: function() {
                    if ($('#panorama-4').is(':empty')) {
                        pannellum.viewer('panorama-4', {
                            "type": "equirectangular",
                            "panorama": "/img/choose/cam-4.jpg",
                            'autoLoad': true,
                            'showControls': false
                        });
                    }
                }
            });
        }

        if (modals[4]) {

            modals[4].animatedModal({
                modalTarget: 'modal-5',
                animatedIn: 'bounceInUp',
                animatedOut: 'zoomOut',
                color: 'white',
                beforeOpen: function() {
                    if ($('#panorama-5').is(':empty')) {
                        pannellum.viewer('panorama-5', {
                            "type": "equirectangular",
                            "panorama": "/img/choose/cam-5.jpg",
                            'autoLoad': true,
                            'showControls': false
                        });
                    }
                }
            });
        }

        if (modals[5]) {

            modals[5].animatedModal({
                modalTarget: 'modal-6',
                animatedIn: 'bounceInUp',
                animatedOut: 'zoomOut',
                color: 'white',
                beforeOpen: function() {
                    if ($('#panorama-6').is(':empty')) {
                        pannellum.viewer('panorama-6', {
                            "type": "equirectangular",
                            "panorama": "/img/choose/cam-6.jpg",
                            'autoLoad': true,
                            'showControls': false
                        });
                    }
                }
            });
        }

        if (modals[6]) {

            modals[6].animatedModal({
                modalTarget: 'modal-7',
                animatedIn: 'zoomIn',
                animatedOut: 'zoomOut',
                color: 'white'
            });
        }

    })();


    // Block-pusher of markers during resize


    (() => {

        let fitblockEl = $('.choose-content__fitblock').get(0);

        if (fitblockEl) {
            try {
                class Fitblock {
                    constructor(el) {
                        this.outer = el;
                        this.inner = el.getElementsByClassName('choose-content__fitblock__inner')[0];
                        this.pusher = this.inner.getElementsByClassName('fitblock__pusher')[0];
                        this.refresh();
                    }

                    refresh() {
                        window.requestAnimationFrame(() => {
                            let outer = this.outer;
                            let inner = this.inner;
                            let pusher = this.pusher;
                            let wrapH = outer.offsetHeight;
                            let wrapW = outer.offsetWidth;

                            const pusherH = pusher.height;
                            const pusherW = pusher.width;
                            let rel = pusherW / pusherH;
                            if (wrapW / pusherW > wrapH / pusherH) {
                                pusher.style.width = `${wrapW}px`;
                                pusher.style.height = "auto";
                                inner.style.marginLeft = `-${wrapW/2}px`;
                                inner.style.marginTop = `-${(wrapW/rel)/2}px`;
                            } else {
                                pusher.style.width = "auto";
                                pusher.style.height = `${wrapH}px`;
                                inner.style.marginLeft = `-${(wrapH*rel)/2}px`;
                                inner.style.marginTop = `-${wrapH/2}px`;
                            }

                            inner.style.visibility = "visible";
                        });
                    }
                }

                let mapFitInstance = new Fitblock(fitblockEl);

                $(window).on('resize orientationchange', function() {
                    mapFitInstance.refresh();
                });
                $(window).on('orientationchange', () => {
                    $('.choose-content__fitblock').scrollLeft(0);
                });
                $(window).trigger('resize');
            } catch (err) {
                console.error(err);
            }
        }

    })();



    // AJAX update of tables

    function moreFlats(houseNum, offset) {

        function getRooms() {
            let arr = [];

            $('aside.sidebar #check-1').is(':checked') ? arr.push(0) : false;
            $('aside.sidebar #check-2').is(':checked') ? arr.push(1) : false;
            $('aside.sidebar #check-3').is(':checked') ? arr.push(2) : false;
            $('aside.sidebar #check-4').is(':checked') ? arr.push(3) : false;

            return arr;
        }

        let house,
            offsetReady = offset,
            loader = $('.loader-genplan'),
            $tablesWrapper = $('.filter .container'),
            opts = {
                get_flats: 'json',
                limit: 10,
                offset: offsetReady,
                house: houseNum,
                priceFrom: $('aside.sidebar [name="price-from"]').val(),
                priceTo: $('aside.sidebar [name="price-to"]').val(),
                priceMeterFrom: $('aside.sidebar [name="price-meter-from"]').val(),
                priceMeterTo: $('aside.sidebar [name="price-meter-to"]').val(),
                floorFrom: $('aside.sidebar [name="floor-from"]').val(),
                floorTo: $('aside.sidebar [name="floor-to"]').val(),
                rooms: getRooms(), //массив
                sqFrom: $('aside.sidebar [name="sq-from"]').val(),
                sqTo: $('aside.sidebar [name="sq-to"]').val(),
                furnish: `${$('aside.sidebar #checkbox-1').is(':checked') ? '1' : '0'}`,
                flatview: `${$('aside.sidebar #checkbox-2').is(':checked') ? '1' : '0'}`,
                discount: `${$('aside.sidebar #checkbox-3').is(':checked') ? '1' : '0'}`
            };

        $.ajax({
            method: "GET",
            url: '/choose/search/',
            data: opts,
            dataType: 'json',
            beforeSend: function() {
                loader.addClass('loader--active');
            }
        }).done((response) => {
            try {
                house = response.houses[0];

            }
            catch(err) {
                console.error(err);
            }

            if (house.flats.length === 0) {
                $(`.show-more[data-table-button="${houseNum - 1}"]`).remove();
                loader.removeClass('loader--active');
                return;
            }

            completeFillTable(house, houseNum - 1);
            customSidebarScroll();
            loader.removeClass('loader--active');

            if(house.flats.length < 10) {
                $(`.show-more[data-table-button="${houseNum - 1}"]`).remove();
            }
        });

    }


    function getFlats() {

        let houses = [],
        markup = [],
        totalFound = 0,
        $totalFound = $('.sidebar__result .sidebar__result__found'),
        $tablesWrapper = $('.sidebar__result__wrapper'),
        loader = $('.loader-genplan');

        function getRooms() {
            let arr = [];

            $('aside.sidebar #check-1').is(':checked') ? arr.push(0) : false;
            $('aside.sidebar #check-2').is(':checked') ? arr.push(1) : false;
            $('aside.sidebar #check-3').is(':checked') ? arr.push(2) : false;
            $('aside.sidebar #check-4').is(':checked') ? arr.push(3) : false;

            return arr;
        }

        let sidebar = $("aside.sidebar"),
            opts = {
                get_flats: 'json',
                priceFrom: $('aside.sidebar [name="price-from"]').val(),
                priceTo: $('aside.sidebar [name="price-to"]').val(),
                priceMeterFrom: $('aside.sidebar [name="price-meter-from"]').val(),
                priceMeterTo: $('aside.sidebar [name="price-meter-to"]').val(),
                floorFrom: $('aside.sidebar [name="floor-from"]').val(),
                floorTo: $('aside.sidebar [name="floor-to"]').val(),
                rooms: getRooms(), //массив
                sqFrom: $('aside.sidebar [name="sq-from"]').val(),
                sqTo: $('aside.sidebar [name="sq-to"]').val(),
                furnish: `${$('aside.sidebar #checkbox-1').is(':checked') ? '1' : '0'}`,
                flatview: `${$('aside.sidebar #checkbox-2').is(':checked') ? '1' : '0'}`,
                discount: `${$('aside.sidebar #checkbox-3').is(':checked') ? '1' : '0'}`
            };

            $.ajax({
                method: "GET",
                url: '/choose/search/',
                data: opts,
                dataType: 'json',
                beforeSend: () => {
                    $($tablesWrapper).empty();
                    $totalFound.html('Поиск...');
                    loader.addClass('loader-genplan--active');
                    customSidebarScroll();
                    $('.sidebar__wrapper').getNiceScroll().resize();
                }
            }).done((response) => {

            try {
                houses = response.houses;
                for(let house of houses) {
                    totalFound += house.total;
                }
            }
            catch(err) {
                console.error(err);
            }

            $($tablesWrapper).empty();
            $totalFound.html(`Найдено ${totalFound} квартир`);

            houses.forEach(function(item, index) {
                if(item.flats.length === 0) return;

                    let $table = createTable(item, index);

                    insertTable($table, $tablesWrapper);
                    makeTableSortable($(`.table[data-table='${index}']`));
                    initFillTable(item, $(`.table[data-table='${index}']`), index);

                $(`.show-more[data-table-button="${index}"]`).unbind('click').click(function(event) {
                    let offset = $(`.table[data-table="${index}"] tbody tr`).get().length;
                    event.preventDefault();
                    moreFlats(index + 1, offset);
                    offset += 10;
                });
            });
            customSidebarScroll();
            loader.removeClass('loader-genplan--active');



            }).fail((err) => {
              throw err;
            });

    }


    // Initialization of sliders in sidebar
    (() => {

        var one = $('#slider-1'),
            two = $('#slider-2'),
            three = $('#slider-3');

        if (one) {

            let val = [$('.sidebar__slider-1').data('min'), $('.sidebar__slider-1').data('max')]
            one.slider({
                min: val[0],
                max: val[1],
                range: true,
                values: [val[0], val[1]],
                slide: function(event, ui) {
                    $('.sidebar__slider-1__inputs input:first-of-type').val(ui.values[0]);
                    $('.sidebar__slider-1__inputs input:last-of-type').val(ui.values[1]);
                },
                stop: function(event, ui) {
						getFlats();
                }
            });
        }

        if (two) {

            let val = [$('.sidebar__slider-2').data('min'), $('.sidebar__slider-2').data('max')]
            two.slider({
                min: val[0],
                max: val[1],
                range: true,
                values: [val[0], val[1]],
                slide: function(event, ui) {
                    $('.sidebar__slider-2__inputs input:first-of-type').val(ui.values[0]);
                    $('.sidebar__slider-2__inputs input:last-of-type').val(ui.values[1]);
                },
                stop: function() {
                	getFlats();
                }
            });
        }

        if (three) {

            let val = [$('.sidebar__slider-3').data('min'), $('.sidebar__slider-3').data('max')]
            three.slider({
                min: val[0],
                max: val[1],
                range: true,
                values: [val[0], val[1]],
                slide: function(event, ui) {
                    $('.sidebar__slider-3__inputs input:first-of-type').val(ui.values[0]);
                    $('.sidebar__slider-3__inputs input:last-of-type').val(ui.values[1]);
                },
                stop: function() {
                	getFlats();
                }
            });
        }

        $('aside.sidebar-choose input').on('change', function() {
            getFlats();
        });


    })();

    // Filter on cha
	( () => {
		getFlats();

	})();

});