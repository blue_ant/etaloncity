function completeFillTable (house, tableIndex, dimension) {
    let $tableDesktop = $(`.table-desktop[data-table="${tableIndex}"]`),
        $tableMobile = $(`.table-mobile[data-table="${tableIndex}"]`);


    if (!house.flats instanceof Array) {
        throw new Error('Flats are not presented as an array');
    } else if (house.flats.length === 0) {
        throw new Error('Flats array is empty');
    }

    if (dimension !== 'mobile') {

        for (let flat of house.flats) {

            let $view = '';

            if (flat.viewUrl.length !== 0) {
                flat.viewUrl.forEach(function(el, index) {
                    $view += `<a class="view-image" ${index >= 1 ? 'hidden' : false} href="${el.src ? el.src : false}" data-fancybox="${flat.id}" data-caption="${el.name ? el.name : false}"><div style="background: url(${el.src ? el.src : false}) no-repeat center; background-size: cover;"></div></a>`;
                });
            }

            $tableDesktop.DataTable().row.add([$view, `${flat.rooms !== 0 ? flat.rooms + ' ком.' : '<span hidden>0</span> Студия'}`, `${house.houseName}`, flat.floor, `${flat.area}`, `${flat.price}`, `${flat.pricem2}`]).draw().node();
            if (flat.discount) {
             $('tbody tr:last-of-type', $tableDesktop).addClass('has-action');
             $('tbody tr:last-of-type', $tableDesktop).addClass('flag');
            }
            if (flat.furnish) {
                $('tbody tr:last-of-type', $tableDesktop).addClass('brush');
            }
            $(`<a class="row-link" href="${flat.url}"></a>`).appendTo($('tbody tr:last-of-type td:last-of-type', $tableDesktop));
        }
    }
    else if (dimension === 'mobile') {

        try {

            for (let flat of house.flats) {

                let view = '';

                if (flat.viewUrl.length !== 0) {
                    flat.viewUrl.forEach(function(el, index) {
                        view += `<a ${index >= 1 ? 'hidden' : false} href="${el.src ? el.src : false}" data-fancybox="${flat.id}" data-caption="${el.name ? el.name : false}"><div style="background: url(${el.src ? el.src : false}) no-repeat center; background-size: cover;"></div></a>`;
                    });
                }


                let $flat = `
                
                <tr>
                    <td>${view}</td>
                    <td>${flat.rooms !== 0 ? flat.rooms + ' ком.' : '<span hidden>0</span> Студия'}</td>
                    <td>${house.houseName}</td>
                    <td>${flat.floor}</td>
                    <td>${flat.area}</td>
                    <td>${splitDigits(flat.price)} руб.</td>
                    <td class="mobile--hidden"></td>
                    <td class="td-section--mobile td--mobile">Секция</td>
                    <td class="td-floor--mobile td--mobile">Этаж</td>
                    <td class="td-area--mobile td--mobile">Площадь</td>
                    <td class="td-link--mobile"><a href="${flat.url}"><div></div></a></td>
                </tr>
                `;

                $tableMobile.find('tbody').append($flat);
                $(`<a class="row-link" href="${flat.url}"></a>`).appendTo($('tbody tr:last-of-type td:last-of-type', $tableMobile));
                if (flat.discount) {
                 $('tbody tr:last-of-type', $tableMobile).addClass('has-action');
                 $('tbody tr:last-of-type', $tableMobile).addClass('flag');
                }
                if (flat.furnish) {
                    $('tbody tr:last-of-type', $tableMobile).addClass('brush');
                }
            }
        }
        catch(err) {
            console.error(err);
        }

    }
}