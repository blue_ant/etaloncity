function createTable (house, index = 'none', dimension) {
    let $table;

    if (dimension !== 'mobile') {

        $table = `

            <table data-table="${index}" class="table table-desktop">
                <thead class="thead">
                    <th data-sort="integer" class="table-th">${house.houseName}</th>
                    <th data-sort="integer" class="table-th" class="th--active">Тип</th>
                    <th data-sort="integer" class="table-th">Секция</th>
                    <th data-sort="integer" class="table-th">Этаж</th>
                    <th data-sort="integer" class="table-th">Площадь</th>
                    <th data-sort="integer" class="table-th">Цена</th>
                    <th data-sort="integer" class="table-th">Стоимость за м<sup>2</sup></th>
                </thead>
                <tbody></tbody>
            </table>
        `;
                    // <th class="table-th">Планировка</th>

        if (house.flats.length > 4) {
          $table += `<a href="" role="button" data-table-button="${index}" data-step="1" class="show-more show-more--desktop">Показать все квартиры</a>`;
        }

    }
    else {
        
        $table = `

            <table data-table="${index}" class="table table-mobile">
                <thead class="thead">
                    <th class="table-th">${house.houseName}</th>
                    <th class="table-th" class="th--active">Тип</th>
                    <th class="table-th">Секция</th>
                    <th class="table-th">Этаж</th>
                    <th class="table-th">Площадь</th>
                    <th class="table-th">Цена</th>
                    <th class="table-th">Стоимость за м<sup>2</sup></th>
                    <th class="table-th table-th-mobile">Свободно ${house.total} кв.</th>
                </thead>
                <tbody></tbody>
            </table>
        `;

        if (house.flats.length > 4) {
            $table += `<a href="" role="button" data-table-button="${index}" data-step="1" class="show-more show-more--mobile">Показать все квартиры</a>`;
        }
    }

    return $table;

}