// =include ../../common/split-digits.js

function makeTableSortable (table) {
    let $table = $(table);

    if (!$($table).hasClass('sorting-inited')) {

        try {
            $($table).DataTable({
                'searching': false,
                'bPaginate': false,
                'bLengthChange': false,
                'bInfo': false,
                "paging": false,
                "searching": false,
                'createdRow': function( row, data, dataIndex ) {

                    $(row).find('td').eq(4).html(data[4] + ' м<sup>2</sup>');
                    $(row).find('td').eq(5).html(splitDigits(parseInt(data[5], 10)) + ' руб.');
                    $(row).find('td').eq(6).html(splitDigits(parseInt(data[6], 10)) + ' руб.');
                    // $(row).find('td').last().append('<a href="#">fff</a>')
                }
            });

            $($table).addClass('sorting-inited');
            $($table).find('thead th').click(function() {
                $(this).siblings().removeClass('th--active');
                $(this).toggleClass('th--active');
            });
        }
        catch(err) {
            console.error(err);
        }
    } else {
        throw new Error('This table is already sortable');
    }
}