(() => {


	// Sliderrs
	for(let i = 1, len = $('[class*="sidebar__slider-"][data-min][data-max]').length; i <= len; i++) {

		$(`.sidebar__slider-${i}__inputs input`).on('change', function() {
			let val = [],
				 checkboxOne = $(`.sidebar__slider-${i}__inputs input:nth-of-type(1)`),
				 checkboxTwo = $(`.sidebar__slider-${i}__inputs input:nth-of-type(2)`);


			if ($(this).prop('name') == checkboxOne.prop('name')) {
				val.push($(this).val());
				val.push(checkboxTwo.val());
			}
			else if ($(this).prop("name") == checkboxTwo.prop('name')) {
				val.push(checkboxOne.val());
				val.push($(this).val());
			}


			store.set(`slider-${i}`, val);
		});

		$(`#slider-${i}`).on('slidestop', function(event, ui) {
			let val = ui.values;
			store.set(`slider-${i}`, val);
		});
	}

	// Rooms
	for (let i = 1, len = $('.sidebar__room__check input').length; i <= len; i++) {
		$(`#check-${i}`).on('change', function() {
			let val = $(this).is(':checked');
			store.set(`room-${i - 1}`, val);
		});
	}

	// Options
	for (let i = 1, len = $('.sidebar__option input').length; i <= len; i++) {
		$(`#checkbox-${i}`).on('change', function() {
			let val = $(this).is(':checked');
			store.set(`option-${i}`, val);
		});
	}


})();