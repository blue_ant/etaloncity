  (() => {

    let flat = $('.flats .flats__container__grid__wrapper__icon:not(.flat--sold):not(.flat--unactive)'),
        popup = $('.flats__popup');

    flat.on('click', function() {

      let flatId = $(this).data('id');
      
      $.ajax({
        method: "GET",
        url: `/choose/`,
        data: {flatId: flatId},
        dataType: 'json'
      })
      .done(function(data) {

        let parsedData = data;

        let $view = '';

        if (parsedData.viewUrl.length !== 0) {
            parsedData.viewUrl.forEach(function(el, index) {
                $view += `<a ${index >= 1 ? 'style="display: none;"' : false} href="${el.src ? el.src : false}" data-fancybox="${flat.id}" data-caption="${el.name ? el.name : false}" class="flats__view"><div style="background: url(${el.src ? el.src : false}) no-repeat center; background-size: cover;"><p>Вид из окна</p></div></a>`;
            });
        }

        // history.pushState({}, 'Выбор квартиры', parsedData.url)

        let filledInputsData = {},
        inputForFilling = $('#form-flat input[name="flat"]'),
        content = 
        `
          <div class="flats__popup--close"></div>

          <div class="flats__popup__container__left">

            <div class="flats__plan-1">
              <img src="${parsedData.planImg ? parsedData.planImg : ''}" alt="">
            </div>

            ${parsedData.planSectionImg ? `<div class="flats__plan-2"><small>${parsedData.house ? parsedData.house : ''}</small><img src="${parsedData.planSectionImg}" alt=""></div>` : ''}
            
            ${parsedData.planFloorImg ? `<div class="flats__plan-3"><small>Этаж</small><img src="${parsedData.planFloorImg ? parsedData.planFloorImg : ''}" alt=""></div>` : ''}

            <ul>
              <li>* Цена дана с учетом скидок</li>
              <li>* Резерв не фиксирует стоимость и условия оплаты</li>
              <li>* Цена действительна при 100% оплате</li>
            </ul>

          </div>
          <div class="flats__popup__container__right ${parsedData.sectionName && parsedData.section ? 'has-section-name' : ''} ${parsedData.viewUrl.length !== 0 ? 'has-image' : ''}">
            <h3>Квартира ${parsedData.num ? parsedData.num : ''}</h3>
            ${parsedData.sectionName && parsedData.section ? `<h4>Секция №${parsedData.section} «${parsedData.sectionName}»</h4>` : ''}
            <div class="flats__flat-info">
              <div class="${parsedData.floor ? '' : 'hidden'}">Этаж:<br/><span>${parsedData.floor ? parsedData.floor : ''}</span></div>
             <div class="${parsedData.rooms ? '' : 'hidden'}">Комнат:<br/><span>${parsedData.rooms ? parsedData.rooms : ''}${parsedData.rooms == 0 ? 'Студия' : ''}</span></div>
             <div class="${parsedData.area ? '' : 'hidden'}">Площадь:<br/><span>${parsedData.area ? parsedData.area : ''} м<sup>2</sup></span></div>
            </div>
            <div class="flats__flat-price">
              <p>Стоимость:</p>
              <p>${splitDigits(parsedData.price) ? splitDigits(parsedData.price) : "0"} руб.*</p>
            </div>

            <div class="flats__buttons-1">
              ${$view}
              <a href="${$('.flats__popup').data('furnish-src')}" class="flats__furniture"><div><p>Расстановка<br/> мебели</p></div></a>
            </div>

            <div class="flats__buttons-2">
              <a href="" class="flats__button-apply">Оставить заявку</a>
              <a href="${parsedData.pdfPath}" target="_blank" class="flats__button-download">Скачать планировку</a>
            </div>

            <ul>
              <li>* Цена дана с учетом скидок</li>
              <li>* Резерв не фиксирует стоимость и условия оплаты</li>
              <li>* Цена действительна при 100% оплате</li>
            </ul>


          </div>
        `;

        popup.find('.flats__popup__container').html(content);
        if( checkDevice() !== 'desktop') {
          $('body, html').css('overflow', 'hidden');
          $('body,html').addClass('noscroll');
        }
        $('.flats__popup--close').unbind('click').on('click', function() {
          if( checkDevice() !== 'desktop') {
            $('body, html').removeAttr('style');
            $('body,html').removeClass('noscroll');
          }
          popup.removeClass("flats__popup--visible");
          history.back();
        });

        popup.addClass("flats__popup--visible");

        for(let item in parsedData) {
          if (item === 'house' || item === 'section' || item === 'sectionName' || item === "num") {
            filledInputsData[item] = parsedData[item];
          }
        }
        inputForFilling.val(`Корпус № ${filledInputsData.house}; Секция № ${filledInputsData.section}; Название секции: ${filledInputsData.sectionName}; Квартира № ${filledInputsData.num}`);
        fancyboxDefaults();


        $('.flats__popup--close').on('click', function(e) {

          if ($('body').hasClass('flat-mode')) {
            
            if (document.referrer.match(/choose\/search/g) !== null) {
              document.location = '/choose/search/';
            } else {
              e.preventDefault();
              let url = window.location.href.split('/');
              url.splice(-2)
              url = url.join('/');

             setTimeout(function() {
               window.history.pushState('ChooseFlat', 'Выбор квартиры', url);
             }, 100)

            }

          }
        });


      }).fail(function() {
        alert('Произошла какая-то ошибка. Попробуйте еще раз.');
      });
    });
  })();