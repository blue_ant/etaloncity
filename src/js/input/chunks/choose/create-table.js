function createTable (house, index = 'none') {
    let $tableMarkup,
        $button;

    $tableMarkup = `

      <p class="sidebar__result__info-1">${house.houseName}</p>
      <p class="sidebar__result__info-2 ${house.finish ? '' : 'hidden'}">Дом сдан</p>
      <p>Свободно ${house.total} кв.</p>

      <div class="table-wrapper">

        <table data-table="${index}" class="table">
          <thead>
            <tr>
              <th>Комнат</th>
              <th>Секция</th>
              <th>Этаж</th>
              <th>Площадь</th>
              <th>Цена*</th>
            </tr>
          </thead>
          <tbody></tbody>
        </table>
    `;

    if (house.flats.length > 4) {
      $button = `
          <a href="" data-table-button="${index}" data-step="1" class="show-more">Показать ещё</a>

        </div>
      `;
    }
    else {
      $button = `</div>`;
    }

    $tableMarkup += $button;

    return $tableMarkup;

}