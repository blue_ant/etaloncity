function completeFillTable (house, tableIndex) {

	let $table = $(`.table[data-table="${tableIndex}"]`);

	try {

	    for (let flat of house.flats) {
	        $(`.table[data-table="${tableIndex}"]`).DataTable().row.add([`${flat.rooms !== 0 ? flat.rooms : '<span hidden>0</span> Студия'}`, house.houseName, flat.floor, flat.area, flat.price]).draw().node();
	          if (flat.discount) {
	           // $('tbody tr:last-of-type', $table).addClass('has-action');
	          }
	    }
	}
	catch(err) {
		console.error(err);
	}

}