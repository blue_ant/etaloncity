	function insertTable (table, wrapper) {
    let $table = $(table),
    $wrapper = $(wrapper);

    if (!$table) {
        throw new Error("You haven't specified table or table doesn't exist");
    } else if (!$wrapper) {
        throw new Error("You haven't specified wrapper or wrapper doesn't exist"); 
    }


    $($table).appendTo($wrapper);
}