function initFillTable(house, table, tableIndex) {
    let $table = $(table);

    if (!house) {
        throw new Error('House doesn\'t exist');
    }

    try {

        for (let flat of house.flats) {
            $table.DataTable().row.add([`${flat.rooms !== 0 ? flat.rooms  : '<span hidden>0</span> Студия'}`, house.houseName, flat.floor, flat.area, flat.price]).draw().node();
                // if (flat.discount) {
                //  $('tbody tr:last-of-type', $table).addClass('has-action');
                // }
        }
    } catch(err) {
        console.error(err);
    }
}