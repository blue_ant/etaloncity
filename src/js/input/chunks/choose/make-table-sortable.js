// =include ../../common/split-digits.js


function makeTableSortable (tableMarkup) {
    let $table = $(tableMarkup);

    if (!$table.hasClass('sorting-inited')) {

        try {
            $table.DataTable({
                'searching': false,
                'bPaginate': false,
                'bLengthChange': false,
                'bInfo': false,
                "paging": false,
                "searching": false,
                'createdRow': function( row, data, dataIndex ) {
                    $(row).find('td').eq(3).html(data[3] + ' м<sup>2</sup>');
                    $(row).find('td').eq(4).html(splitDigits(parseInt(data[4], 10)));
                }
            });


            $table.addClass('sorting-inited');
        }
        catch(err) {
            console.error(err);
        }
    } else {
        throw new Error('This table is already sortable');
    }
}