$(document).ready(function() {
    let mapOne = new GMaps({
        div: '.contact_map_1',
        zoom: 14,
        lat: 55.559242,
        lng: 37.555276,
        styles: [{ "stylers": [{ "saturation": 50 }, { "lightness": 50 }, { "visibility": "on" }, { "weight": 2 }] }, { "elementType": "geometry", "stylers": [{ "color": "#ebe3cd" }, { "saturation": -45 }] }, { "elementType": "labels.icon", "stylers": [{ "saturation": 5 }, { "weight": 3.5 }] }, { "elementType": "labels.text.fill", "stylers": [{ "color": "#523735" }] }, { "elementType": "labels.text.stroke", "stylers": [{ "color": "#e7e2d9" }] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{ "color": "#c9b2a6" }] }, { "featureType": "administrative.land_parcel", "elementType": "geometry.stroke", "stylers": [{ "color": "#dcd2be" }] }, { "featureType": "administrative.land_parcel", "elementType": "labels.text.fill", "stylers": [{ "color": "#ae9e90" }] }, { "featureType": "landscape", "elementType": "labels.text", "stylers": [{ "saturation": -20 }, { "weight": 2 }] }, { "featureType": "landscape", "elementType": "labels.text.fill", "stylers": [{ "weight": 3 }] }, { "featureType": "landscape.natural", "elementType": "geometry", "stylers": [{ "color": "#dfd2ae" }] }, { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#dfd2ae" }] }, { "featureType": "poi", "elementType": "labels.text.fill", "stylers": [{ "color": "#93817c" }] }, { "featureType": "poi.park", "elementType": "geometry.fill", "stylers": [{ "color": "#a5b076" }] }, { "featureType": "poi.park", "elementType": "labels.text.fill", "stylers": [{ "color": "#1f3516" }] }, { "featureType": "road", "stylers": [{ "saturation": 50 }, { "lightness": 70 }, { "weight": 1 }] }, { "featureType": "road", "elementType": "geometry", "stylers": [{ "color": "#f5f1e6" }] }, { "featureType": "road.arterial", "stylers": [{ "weight": 2.5 }] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "color": "#fdfcf8" }] }, { "featureType": "road.highway", "elementType": "geometry", "stylers": [{ "color": "#f89d67" }] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "color": "#eec064" }] }, { "featureType": "road.highway.controlled_access", "elementType": "geometry", "stylers": [{ "color": "#e98d58" }] }, { "featureType": "road.highway.controlled_access", "elementType": "geometry.stroke", "stylers": [{ "color": "#d38152" }] }, { "featureType": "road.local", "stylers": [{ "color": "#e6ce8b" }] }, { "featureType": "road.local", "elementType": "labels.icon", "stylers": [{ "visibility": "simplified" }] }, { "featureType": "road.local", "elementType": "labels.text", "stylers": [{ "color": "#c6ac86" }] }, { "featureType": "road.local", "elementType": "labels.text.fill", "stylers": [{ "color": "#806b63" }] }, { "featureType": "transit.line", "elementType": "geometry", "stylers": [{ "color": "#dfd2ae" }] }, { "featureType": "transit.line", "elementType": "labels.text.fill", "stylers": [{ "color": "#8f7d77" }] }, { "featureType": "transit.line", "elementType": "labels.text.stroke", "stylers": [{ "color": "#ebe3cd" }] }, { "featureType": "transit.station", "elementType": "geometry", "stylers": [{ "color": "#dfd2ae" }] }, { "featureType": "water", "elementType": "geometry.fill", "stylers": [{ "color": "#b9d3c2" }] }, { "featureType": "water", "elementType": "labels.text.fill", "stylers": [{ "color": "#92998d" }] }]
    })

    mapOne.addMarker({
        lat: 55.559000,
        lng: 37.556907,
        title: 'Офис продаж',
        icon: {
            url: '/img/adjustment/marker_map_1.png',
            scaledSize: new google.maps.Size(68, 82)
        },
        details: {
            type: 500
        },
        infoWindow: {
            content: 'Офис продаж'
        }
    })

    mapOne.drawRoute({
        origin: [55.581468, 37.569988],
        destination: [55.558990, 37.556939],
        travelMode: 'driving',
        strokeColor: "#bd3af8",
        strokeOpacity: 1,
        strokeWeight: 4
    })


    let mapTwo = new GMaps({
        div: '.contact_map_2',
        zoom: 14,
        lat: 55.559242,
        lng: 37.555276,
        styles: [{ "stylers": [{ "saturation": 50 }, { "lightness": 50 }, { "visibility": "on" }, { "weight": 2 }] }, { "elementType": "geometry", "stylers": [{ "color": "#ebe3cd" }, { "saturation": -45 }] }, { "elementType": "labels.icon", "stylers": [{ "saturation": 5 }, { "weight": 3.5 }] }, { "elementType": "labels.text.fill", "stylers": [{ "color": "#523735" }] }, { "elementType": "labels.text.stroke", "stylers": [{ "color": "#e7e2d9" }] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{ "color": "#c9b2a6" }] }, { "featureType": "administrative.land_parcel", "elementType": "geometry.stroke", "stylers": [{ "color": "#dcd2be" }] }, { "featureType": "administrative.land_parcel", "elementType": "labels.text.fill", "stylers": [{ "color": "#ae9e90" }] }, { "featureType": "landscape", "elementType": "labels.text", "stylers": [{ "saturation": -20 }, { "weight": 2 }] }, { "featureType": "landscape", "elementType": "labels.text.fill", "stylers": [{ "weight": 3 }] }, { "featureType": "landscape.natural", "elementType": "geometry", "stylers": [{ "color": "#dfd2ae" }] }, { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#dfd2ae" }] }, { "featureType": "poi", "elementType": "labels.text.fill", "stylers": [{ "color": "#93817c" }] }, { "featureType": "poi.park", "elementType": "geometry.fill", "stylers": [{ "color": "#a5b076" }] }, { "featureType": "poi.park", "elementType": "labels.text.fill", "stylers": [{ "color": "#1f3516" }] }, { "featureType": "road", "stylers": [{ "saturation": 50 }, { "lightness": 70 }, { "weight": 1 }] }, { "featureType": "road", "elementType": "geometry", "stylers": [{ "color": "#f5f1e6" }] }, { "featureType": "road.arterial", "stylers": [{ "weight": 2.5 }] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "color": "#fdfcf8" }] }, { "featureType": "road.highway", "elementType": "geometry", "stylers": [{ "color": "#f89d67" }] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "color": "#eec064" }] }, { "featureType": "road.highway.controlled_access", "elementType": "geometry", "stylers": [{ "color": "#e98d58" }] }, { "featureType": "road.highway.controlled_access", "elementType": "geometry.stroke", "stylers": [{ "color": "#d38152" }] }, { "featureType": "road.local", "stylers": [{ "color": "#e6ce8b" }] }, { "featureType": "road.local", "elementType": "labels.icon", "stylers": [{ "visibility": "simplified" }] }, { "featureType": "road.local", "elementType": "labels.text", "stylers": [{ "color": "#c6ac86" }] }, { "featureType": "road.local", "elementType": "labels.text.fill", "stylers": [{ "color": "#806b63" }] }, { "featureType": "transit.line", "elementType": "geometry", "stylers": [{ "color": "#dfd2ae" }] }, { "featureType": "transit.line", "elementType": "labels.text.fill", "stylers": [{ "color": "#8f7d77" }] }, { "featureType": "transit.line", "elementType": "labels.text.stroke", "stylers": [{ "color": "#ebe3cd" }] }, { "featureType": "transit.station", "elementType": "geometry", "stylers": [{ "color": "#dfd2ae" }] }, { "featureType": "water", "elementType": "geometry.fill", "stylers": [{ "color": "#b9d3c2" }] }, { "featureType": "water", "elementType": "labels.text.fill", "stylers": [{ "color": "#92998d" }] }]
    })

    mapTwo.addMarker({
        lat: 55.548367,
        lng: 37.555826,
        title: 'Офис продаж',
        icon: {
            url: '/img/adjustment/marker_map_3.png',
            scaledSize: new google.maps.Size(68, 82)
        },
        details: {
            type: 500
        },
        infoWindow: {
            content: 'Офис продаж'
        }
    })

    mapTwo.addMarker({
        lat: 55.558990,
        lng: 37.556939,
        title: 'Метро',
        icon: {
            url: '/img/adjustment/marker_map_2.png',
            scaledSize: new google.maps.Size(68, 82)
        },
        details: {
            type: 500
        },
        infoWindow: {
            content: 'Метро'
        }
    })

    mapTwo.drawRoute({
        origin: [55.558990, 37.556939],
        destination: [55.548367, 37.555826],
        travelMode: 'driving',
        strokeColor: "#3da1fd",
        strokeOpacity: 1,
        strokeWeight: 4
    })
})