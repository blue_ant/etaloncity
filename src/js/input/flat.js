$(document).ready(function() {
  // =include common/check-device.js
  ( () => {

    var mySwiper = new Swiper('.swiper-container', {
        effect: 'fade',
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
        on: {
          slideChangeTransitionStart: function() {

            $(".flat-content__counter span").html(this.activeIndex + 1);
          },
          init: function() {
            
            $(".flat-content__counter span").html('1');
            $(".flat-content__counter small").html(`/ ${this.slides.length}`);
          }
        }
    });

  })();

  ( () => {
    var footer = $('footer.footer');

    footer.css({
      'bottom' : '-' + $('footer.footer').height() + 'px'
    });

    $(window).resize(() => {
        footer.css({
        'bottom' : '-' + $('footer.footer').height() + 'px'
      });
    })
  })();


});