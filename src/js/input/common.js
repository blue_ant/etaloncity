$(document).ready(function() {

    // =include common/check-device.js

    // COMMON ———————————————————————————————————————————————————————————————



    $('[data-src="#more-works"]').fancybox({
        smallBtn: false,
        touch: false,
        buttons: [],
        beforeShow: () => {
            $('.more-works').addClass('more-works--active');
            $('.fancybox-slide').addClass('more-works-slide--active');
            $('.fancybox-container').addClass('fancybox-container--active');
        },
        afterClose: () => {
            $('.more-works').removeClass('more-works--active');
        },
        onActivate: () => {
            $('.fancybox-stage').css('pointerEvents', 'all');
            $('.fancybox-toolbar').hide();
        }
    });


    // HEADER —————————————————————————————————————————————————————————————————————————————————

    // Navigation drop

    (() => {

        if ($('.nav-dropped')) {
            try {
                
                let $initialElem = $('.nav-dropped'),
                    $otherElems = $('.sub-trigger'),
                    $background = $('.sub-menu--background'),
                    visibleByDefault = false,
                    state = false;

                if (document.querySelector('.sub-trigger.nav-dropped')) {
                     $background.addClass('active');
                     visibleByDefault = true;
                }

                $otherElems.on('mouseover', function() {

                    if ($(this).hasClass('nav-dropped')) {
                        return;
                    }
                    else {
                        $('.nav-dropped').removeClass('nav-dropped');
                        $(this).addClass('nav-dropped');
                        $background.addClass('active');
                    }
                });

                $('.header .sub-menu').on('mouseover', function() {
                    if (!visibleByDefault) {
                        $background.addClass('active');
                    }
                })

                $('.nav__wrapper .sub-menu').on('mouseover', function() {
                    state = false;
                    $initialElem.removeClass('nav-dropped')
                });

                $('.nav__wrapper .sub-menu').on('mouseout', function() {
                    state = true;
                });

                $('.nav__wrapper ul').on('mouseout', function() {
                    if (state) {

                        $otherElems.removeClass('nav-dropped');
                        $initialElem.addClass('nav-dropped');
                        
                        if(!visibleByDefault) {
                            $background.removeClass('active');
                        }
                    }
                });

            }
            catch(err) {
                console.error(err);
            }
        }



    })();


    // headerClass by width of device
    (() => {

        let winWidth = $(window).width(),
            el = $('.header .container');

        $(window).resize(() => {

            if (checkDevice() === 'desktop') {
                el.removeClass('tablet mobile');
                el.addClass('desktop');
                // $('.logo-mobile').removeClass('hidden');
                // $('.logo-desktop').addClass('hidden');
            } else if (checkDevice() !== 'desktop' && checkDevice() === 'mobile') {
                el.removeClass('desktop mobile');
                el.addClass('tablet');
                // $('.logo-mobile').addClass('hidden');
                // $('.logo-desktop').removeClass('hidden');
            } else if (checkDevice() === 'mobile') {
                el.removeClass('tablet desktop');
                el.addClass('mobile');
            }

        });

    })();


    //toggle submenu
    // ++++++++++

    $('nav .nav__wrapper > ul > li > a').on('click', function(e) {
        let sub = $('nav .sub-menu'),
            thisSub = $(this).parent().find('.sub-menu'),
            subHeight = $(this).siblings('.sub-menu').height(),
            subHeightCollapsed = 38

        if ($(thisSub).length === 0) {
            return;
        }

        if ($(window).width() < 1240) {

            e.preventDefault();

            // $('.nav .nav__wrapper > ul > li').parent().css({
            //     'height': '38px'
            // });

            if (!thisSub.hasClass('submenu--on')) {
                sub.removeClass('submenu--on');
                thisSub.addClass('submenu--on');

                sub.parent().css({
                    'height': subHeightCollapsed + 'px'
                });

                $(this).parent().css({
                    'height': (subHeight + subHeightCollapsed) + 'px'
                });
            } else if (thisSub.hasClass('submenu--on')) {

                thisSub.removeClass('submenu--on');
                $(this).parent().css({
                    'height': subHeightCollapsed + 'px'
                });
            }
        }
    });


    //toggle menu

    $('.header .hamburger').on('click', function(e) {
        e.stopPropagation();

        $(this).toggleClass('hamburger--active');
        $('.header nav').toggleClass('menu--mobile--on');
        $('nav .nav__wrapper .sub-menu').removeClass('submenu--on');
        $('nav .nav__wrapper .sub-menu').parent().css({
            'height': '38px'
        });
        $('html, body').toggleClass('noscroll');

    });

    // FOOTER —————————————————————————————————————————————————————————————————

    let isFullscreen = $('body').hasClass('page--fullscreen');

    if (isFullscreen) {

        var footer = $('.footer');
        $('body').mousewheel((event) => {
            if (checkDevice() === 'desktop') {
                if (event.deltaY < 0) {
                    footer.css({
                        'transform': 'translate3d(0,-' + footer.height() + 'px,0)'
                    });
                } else if (event.deltaY > 0) {
                    footer.css({
                        'transform': 'translate3d(0,0,0)'
                    });
                }
            }
        });

        if (checkDevice() === 'tablet') {

            if ($('body').hasClass('page--fullscreen')) {

                $('body, html').on('swipeup', function() {
                    footer.css({
                        'transform': 'translate3d(0,-' + footer.height() + 'px,0)'
                    });
                });

                $('body, html').on('swipedown', function() {
                    footer.css({
                        'transform': 'translate3d(0,0,0)'
                    });
                });
            }

        }

        $(window).resize(() => {

            footer.css({
                'transform': 'translate3d(0,0,0)'
            });

        });

        (() => {
            var footer = $('footer.footer');

            footer.css({
                'bottom': '-' + $('footer.footer').height() + 'px'
            });

            $(window).resize(() => {
                footer.css({
                    'bottom': '-' + $('footer.footer').height() + 'px'
                });
            });
        })();


        (() => {
            var el = $('.header ~ [class*="content"]');

            if ($('body').hasClass('page--fullscreen')) {
                
                if (checkDevice() === 'mobile') {
                    $('body, html').on('swipeup', function() {
                        el.css({
                            'transform': 'translateY(-100vh)'
                        });
                    });

                    $('body, html').on('swipedown', function() {
                        el.css({
                            'transform': 'translateY(0vh)'
                        });
                    });
                }
            }


        })();

        if ($('.owl-carousel-under')) {
            $('.owl-carousel-under').owlCarousel({
                dots: false,
                nav: true,
                items: 1,
                navText: false,
                touchDrag: true,
                mouseDrag: true,
                loop: true
            });
        }

        $('.fullscreen-mobile--hidden').removeClass('fullscreen-mobile--hidden');

    }


    (() => {

        if ($('section.filter').length === 0  && $('.flat-mode').length === 0) {
            localStorage.clear();
        }

    })();


});