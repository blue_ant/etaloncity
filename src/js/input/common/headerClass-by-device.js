( () => {

  let winWidth = $(window).width(),
  el = $('.header .container');

  $(window).resize( () => {

    if(checkDevice() === 'desktop') {
      el.removeClass('tablet mobile');
      el.addClass('desktop');
      // $('.logo-mobile').removeClass('hidden');
      // $('.logo-desktop').addClass('hidden');
    }
    else if (checkDevice() !== 'desktop' && checkDevice() === 'mobile') {
      el.removeClass('desktop mobile');
      el.addClass('tablet');
      // $('.logo-mobile').addClass('hidden');
      // $('.logo-desktop').removeClass('hidden');
    }
    else if (checkDevice() === 'mobile') {
      el.removeClass('tablet desktop');
      el.addClass('mobile');
    }

  });

})();