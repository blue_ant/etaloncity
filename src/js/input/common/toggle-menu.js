$('.header .hamburger').on('click', function(e) {
	e.stopPropagation();

    $(this).toggleClass('hamburger--active');
    $('.header nav').toggleClass('menu--mobile--on');
    $('nav .nav__wrapper .sub-menu').removeClass('submenu--on');

});