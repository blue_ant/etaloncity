$('nav .nav__wrapper > ul > li > a').on('click', function(e) {
    let sub = $('nav .sub-menu'),
    thisSub = $(this).parent().find('.sub-menu');

    if($(window).width() < 1024 ) {
      e.preventDefault();
      sub.removeClass('submenu--on');
      if(!thisSub.hasClass('submenu--on')) {
        thisSub.addClass('submenu--on');
      }
      else if (thisSub.hasClass('submenu--on')) {
        thisSub.removeClass('submenu--on');
      }
    }
  });