$(document).ready(function() {


    // =include common/headerClass-by-device.js
    // =include common/check-device.js
    // =include common/split-digits.js

    let dimension = checkDevice();

    // =include chunks/city/*.js 

  function splitDigits( num ) {
      var str = num.toString().split('.');
      if (str[0].length >= 5) {
          str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1 ');
      }
      if (str[1] && str[1].length >= 5) {
          str[1] = str[1].replace(/(\d{3})/g, '$1 ');
      }
      return str.join('.');
  }

  ( () => {

    $('#owl-carousel').owlCarousel({
      dots: false,
      nav: false,
      items: 1,
      navText: false,
      touchDrag: false,
      mouseDrag: false,
      autoplay: true,
      loop: true,
      autoplayTimeout: 4000,
      autoplaySpeed: 5000
    });

  })();


  // Automated popup revealing after coming from filter page
  ( () => {

    let el = $('.flats__container__grid__wrapper__icon.current'),
    interval = setInterval(() => {
      if(el) {
        el.click();
        clearInterval(interval);
      }
      else {
        clearInterval(interval);
      }
    });
    
  })();




( () => {

  let elem = $('.grid-total');

  elem.html(`Свободно ${$('.flats__container__grid__wrapper__icon').length} квартиры`)


})();



  // Svg korpus tooltips
  ( () => {
    let text;

    $(document).tooltip({
        track: true,
        items: 'polygon',
        classes: {
          "ui-tooltip": "korpus-tooltip"
        },
        show: {
          duration: 100
        },
        hide: {
          duration: 100
        },
        open: (event, ui) => {
          text = $(ui.tooltip).text();
          $(ui.tooltip).empty().append(`<p>${text}</p>`);
        }
    });

  })();

  // SVG POLYGONS HOVER (House plan)

  (() => {
    let oldColor;

    $('#korpus-plan polygon').on('mouseover', function(){
      oldColor = $(this).css('fill');
      $(this).css('fill', $(this).data('hover-color'));
    });
    $('#korpus-plan polygon').on('mouseout', function(){

      $(this).css('fill', oldColor);
    });

  })();

  // Lightbox—————————————————————————

  function fancyboxDefaults() {

      $('.flats__view[data-fancybox]').fancybox({
          touch: false,
          idleTime: 0,
          margin: [40, 40],
          buttons: ['close'],
          afterLoad: function afterLoad() {
              var imgWrapper = $('.fancybox-container .fancybox-image-wrap'),
                  el = $('.fancybox-container .fancybox-caption-wrap');

              imgWrapper.prepend(el);
              $('.fancybox-container').addClass('fancybox-container-flatview');
          },
          afterShow: function afterShow() {
              var imgWrapper = $('.fancybox-container .fancybox-image-wrap'),
                  el = $('.fancybox-container .fancybox-navigation');

              el.addClass('fancybox-navigation-visible');
              $('.fancybox-container .fancybox-caption-wrap').addClass('fancybox-caption-wrap--show');
              $('.fancybox-container .fancybox-navigation .count-wrapper').remove();
              $('.fancybox-container .fancybox-navigation').prepend('<div class="count-wrapper"><span class="image-index">' + (this.index + 1) + '</span>/<span class="image-total"> / ' + $('.fancybox-image').length + '</span></div>');
          },
          beforeClose: function beforeClose() {
              $('.fancybox-navigation-visible').removeClass('fancybox-navigation-visible');
          }
      });
  }

  (() => {
    $('.flats__container__content__more .fancybox-trigger').fancybox({

        idleTime: 0,
        margin: [40, 40],
        buttons: ['close'],
        clickContent: false,
        afterLoad: () => {
          $('.fancybox-container').addClass('fancybox-container-furnish');
        }
    });
  })();

  (() => {

    $(document).click(function(e) {

      if($(e.target).hasClass('flats__button-apply')) {
        $('#form-flat').modal({
          close: true,
          overlayClose: true
        });
        return false;
      }
    });
  })();
  //———————————————————————————————


  // Flat popup
  ( () => {

    var popup = $('.flats__popup');

    $('.flats__popup--close').on('click', function() {
      if( checkDevice() !== 'desktop') {
        $('body, html').removeAttr('style');
      }
      popup.removeClass("flats__popup--visible");
      $('body,html').removeClass('noscroll');
    });

  })();

  
});