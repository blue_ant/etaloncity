$(document).ready(function() {

	// =include common/check-device.js

	( () => {
		$('#owl-carousel').owlCarousel({
		  dots: false,
		  nav: true,
		  items: 1,
		  navText: false,
		  touchDrag: true,
		  mouseDrag: true,
		  loop: true
		});
	})();

( () => {
  var footer = $('footer.footer');

  footer.css({
    'bottom' : '-' + $('footer.footer').height() + 'px'
  });
})();


});